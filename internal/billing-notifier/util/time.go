package util

import (
	"time"
)

type TimePeriod [2]time.Time

func (t *TimePeriod) Start() time.Time {

	return t[0]
}

func (t *TimePeriod) End() time.Time {

	return t[1]
}

func CreateTimePeriod(start time.Time, end time.Time) TimePeriod {

	return TimePeriod{start, end}
}

type Clock interface {
	Now() time.Time
}

type realClock struct{}

func (realClock) Now() time.Time { return time.Now() }

var ClockSource Clock

func init() {
	ClockSource = realClock{}
}

func Today() time.Time {

	return ClockSource.Now()
}

func FirstDayOfMonth() time.Time {

	year, month, _ := Today().Date()
	return time.Date(year, month, 1, 0, 0, 0, 0, time.Local)
}

func Yesterday() time.Time {

	today := Today()

	if today.Day() == 1 {
		return today.Local()
	}

	return today.AddDate(0, 0, -1).Local()
}

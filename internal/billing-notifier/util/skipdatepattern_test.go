package util

import (
	"testing"
	"time"
)

func TestParseSkipDatePatternExpectWeekend(t *testing.T) {

	parser := newSkipDatePatternParser()
	var actual SkipDatePattern

	actual, _ = parser.Parse("saturday")
	if actual != WEEKEND {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND)
	}
	actual, _ = parser.Parse("sunday")
	if actual != WEEKEND {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND)
	}
	actual, _ = parser.Parse("Saturday")
	if actual != WEEKEND {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND)
	}
	actual, _ = parser.Parse("Sunday")
	if actual != WEEKEND {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND)
	}
	actual, _ = parser.Parse("SaturDay")
	if actual != WEEKEND {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND)
	}
	actual, _ = parser.Parse("SunDay")
	if actual != WEEKEND {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND)
	}
	actual, _ = parser.Parse("土曜日")
	if actual != WEEKEND {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND)
	}
	actual, _ = parser.Parse("日曜日")
	if actual != WEEKEND {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND)
	}
}

func TestParseSkipDatePatternExpectHoliday(t *testing.T) {

	parser := newSkipDatePatternParser()
	var actual SkipDatePattern

	actual, _ = parser.Parse("holiday")
	if actual != HOLIDAY {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, HOLIDAY)
	}
	actual, _ = parser.Parse("Holiday")
	if actual != HOLIDAY {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, HOLIDAY)
	}
	actual, _ = parser.Parse("HoliDay")
	if actual != HOLIDAY {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, HOLIDAY)
	}
	actual, _ = parser.Parse("祝日")
	if actual != HOLIDAY {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, HOLIDAY)
	}
}

func TestParseSkipDatePatternExpectWeekendOrHoliday(t *testing.T) {

	parser := newSkipDatePatternParser()
	var actual SkipDatePattern

	actual, _ = parser.Parse("saturday,holiday")
	if actual != WEEKEND_OR_HOLIDAY {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND_OR_HOLIDAY)
	}
	actual, _ = parser.Parse("sunday,holiday")
	if actual != WEEKEND_OR_HOLIDAY {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND_OR_HOLIDAY)
	}
	actual, _ = parser.Parse("土曜日,holiday")
	if actual != WEEKEND_OR_HOLIDAY {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND_OR_HOLIDAY)
	}
	actual, _ = parser.Parse("sunday,祝日")
	if actual != WEEKEND_OR_HOLIDAY {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND_OR_HOLIDAY)
	}
}

func TestParseSkipDatePatternSpecificDate(t *testing.T) {

	var actual SkipDatePattern
	var specificDate *time.Time

	parser := newSkipDatePatternParser()

	actual, specificDate = parser.Parse("2018-10-12")
	if actual != UNDEFINED {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, UNDEFINED)
	}
	if specificDate == nil {
		t.Errorf("specificDate is nil")
	}
	if specificDate.Year() != 2018 || specificDate.Month() != 10 || specificDate.Day() != 12 {
		t.Errorf("parse error: actual [%v] expected [%v]", specificDate, "2018-10-12")
	}

	actual, specificDate = parser.Parse("sunday, 2018-10-12")
	if actual != WEEKEND {
		t.Errorf("parse error: actual [%v] expected [%v]", actual, WEEKEND)
	}
	if specificDate == nil {
		t.Errorf("specificDate is nil")
	}
	if specificDate.Year() != 2018 || specificDate.Month() != 10 || specificDate.Day() != 12 {
		t.Errorf("parse error: actual [%v] expected [%v]", specificDate, "2018-10-12")
	}
}

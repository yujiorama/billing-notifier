package util

import (
	"time"

	ics "github.com/PuloV/ics-golang"
)

const (
	japaneseHolidayCalendarPublicURL string = "https://calendar.google.com/calendar/embed?src=ja.japanese%23holiday%40group.v.calendar.google.com&ctz=Asia%2FTokyo"
)

func IsHoliday(t time.Time) bool {

	parser := ics.New()

	parser.GetInputChan() <- japaneseHolidayCalendarPublicURL

	parser.Wait()

	calendars, err := parser.GetCalendars()

	if err != nil {
		return false
	}

	if len(calendars) != 1 {
		return false
	}

	events, err := calendars[0].GetEventsByDate(t)

	if err != nil {
		return false
	}

	return len(events) > 0
}

package util

import (
	"os"
	"regexp"
	"strings"
	"time"
)

type SkipDatePattern int64

const (
	WEEKDAY SkipDatePattern = iota + 1
	WEEKEND
	HOLIDAY
	WEEKDAY_OR_WEEKEND
	WEEKDAY_OR_HOLIDAY
	WEEKDAY_OR_WEEKEND_OR_HOLIDAY
	WEEKEND_OR_HOLIDAY
	UNDEFINED
)

type SkipDatePatternParser interface {
	Parse(s string) (SkipDatePattern, *time.Time)
}

type skipDatePatternParser struct{}

func newSkipDatePatternParser() SkipDatePatternParser {
	return &skipDatePatternParser{}
}

func ParseSkipDatePattern() (SkipDatePattern, *time.Time) {

	skipDatePattern := os.Getenv("SKIP_DATE_PATTERN")

	return newSkipDatePatternParser().Parse(skipDatePattern)
}

func (parser *skipDatePatternParser) Parse(skipDatePattern string) (SkipDatePattern, *time.Time) {

	if len(skipDatePattern) == 0 {
		return UNDEFINED, nil
	}

	weekendPattern := regexp.MustCompile(`saturday|sunday|土曜日|日曜日`)
	holidayPattern := regexp.MustCompile(`holiday|祝日`)
	result := UNDEFINED
	var skipDate *time.Time

	for _, pattern := range strings.Split(skipDatePattern, ",") {
		value := strings.ToLower(strings.TrimSpace(pattern))
		if weekendPattern.MatchString(value) {
			switch result {
			case UNDEFINED, WEEKEND:
				result = WEEKEND
			case HOLIDAY, WEEKEND_OR_HOLIDAY:
				result = WEEKEND_OR_HOLIDAY
			case WEEKDAY, WEEKDAY_OR_WEEKEND:
				result = WEEKDAY_OR_WEEKEND
			case WEEKDAY_OR_HOLIDAY:
				result = WEEKDAY_OR_WEEKEND_OR_HOLIDAY
			}
		}
		if holidayPattern.MatchString(value) {
			switch result {
			case UNDEFINED, HOLIDAY:
				result = HOLIDAY
			case WEEKEND, WEEKEND_OR_HOLIDAY:
				result = WEEKEND_OR_HOLIDAY
			case WEEKDAY, WEEKDAY_OR_HOLIDAY:
				result = WEEKDAY_OR_HOLIDAY
			case WEEKDAY_OR_WEEKEND:
				result = WEEKDAY_OR_WEEKEND_OR_HOLIDAY
			}
		}
		if t, err := time.Parse("2006-01-02", value); err == nil {
			skipDate = &t
		}
	}

	return result, skipDate
}

func (pattern SkipDatePattern) IsSkipDate(t time.Time) bool {

	switch pattern {
	case UNDEFINED:
		return false
	case WEEKEND:
		if t.Weekday() == time.Saturday {
			return true
		}

		if t.Weekday() == time.Sunday {
			return true
		}

		return false
	case HOLIDAY:
		return IsHoliday(t)

	case WEEKEND_OR_HOLIDAY:
		return WEEKEND.IsSkipDate(t) || HOLIDAY.IsSkipDate(t)
	default:
		// TODO WEEKDAY
		return false
	}
}

package util_test

import (
	"os"
	"reflect"
	"testing"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/util"
)

type mockClock struct {
	t time.Time
}

func (m mockClock) Now() time.Time { return m.t }

func newMockClock(t time.Time) mockClock {
	return mockClock{t: t}
}

func timeOf(s string) time.Time {

	t, _ := time.Parse(time.RFC3339, s)

	return t
}

func TestMain(m *testing.M) {
	originalClock := util.ClockSource
	exitCode := m.Run()
	util.ClockSource = originalClock
	os.Exit(exitCode)
}

func TestFirstDayOfMonth(t *testing.T) {
	tests := []struct {
		name string
		now  time.Time
		want time.Time
	}{
		{
			name: "2020-01-01",
			now:  timeOf("2020-01-01T00:00:00+09:00"),
			want: timeOf("2020-01-01T00:00:00+09:00"),
		},
		{
			name: "2020-01-02",
			now:  timeOf("2020-01-02T00:00:00+09:00"),
			want: timeOf("2020-01-01T00:00:00+09:00"),
		},
		{
			name: "2020-01-01",
			now:  timeOf("2020-01-01T23:59:59+09:00"),
			want: timeOf("2020-01-01T00:00:00+09:00"),
		},
		{
			name: "2020-02-01",
			now:  timeOf("2020-02-01T00:00:00+09:00"),
			want: timeOf("2020-02-01T00:00:00+09:00"),
		},
		{
			name: "2020-03-31",
			now:  timeOf("2020-03-31T23:59:59+09:00"),
			want: timeOf("2020-03-01T00:00:00+09:00"),
		},
		{
			name: "2020-04-01",
			now:  timeOf("2020-04-01T00:00:00+09:00"),
			want: timeOf("2020-04-01T00:00:00+09:00"),
		},
		{
			name: "2020-04-02",
			now:  timeOf("2020-04-02T00:00:00+09:00"),
			want: timeOf("2020-04-01T00:00:00+09:00"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			util.ClockSource = newMockClock(tt.now)
			if got := util.FirstDayOfMonth(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FirstDayOfMonth() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToday(t *testing.T) {
	tests := []struct {
		name string
		now  time.Time
		want time.Time
	}{
		{
			name: "2020-01-01",
			now:  timeOf("2020-01-01T00:00:00+09:00"),
			want: timeOf("2020-01-01T00:00:00+09:00"),
		},
		{
			name: "2020-01-02",
			now:  timeOf("2020-01-02T00:00:00+09:00"),
			want: timeOf("2020-01-02T00:00:00+09:00"),
		},
		{
			name: "2020-01-01",
			now:  timeOf("2020-01-01T23:59:59+09:00"),
			want: timeOf("2020-01-01T23:59:59+09:00"),
		},
		{
			name: "2020-02-01",
			now:  timeOf("2020-02-01T00:00:00+09:00"),
			want: timeOf("2020-02-01T00:00:00+09:00"),
		},
		{
			name: "2020-03-31",
			now:  timeOf("2020-03-31T23:59:59+09:00"),
			want: timeOf("2020-03-31T23:59:59+09:00"),
		},
		{
			name: "2020-04-01",
			now:  timeOf("2020-04-01T00:00:00+09:00"),
			want: timeOf("2020-04-01T00:00:00+09:00"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			util.ClockSource = newMockClock(tt.now)
			if got := util.Today(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Today() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestYesterday(t *testing.T) {
	tests := []struct {
		name string
		now  time.Time
		want time.Time
	}{
		{
			name: "2020-01-01",
			now:  timeOf("2020-01-01T00:00:00+09:00"),
			want: timeOf("2020-01-01T00:00:00+09:00"),
		},
		{
			name: "2020-01-02",
			now:  timeOf("2020-01-02T00:00:00+09:00"),
			want: timeOf("2020-01-01T00:00:00+09:00"),
		},
		{
			name: "2020-01-01",
			now:  timeOf("2020-01-01T23:59:59+09:00"),
			want: timeOf("2020-01-01T23:59:59+09:00"),
		},
		{
			name: "2020-02-01",
			now:  timeOf("2020-02-01T00:00:00+09:00"),
			want: timeOf("2020-02-01T00:00:00+09:00"),
		},
		{
			name: "2020-03-31",
			now:  timeOf("2020-03-31T23:59:59+09:00"),
			want: timeOf("2020-03-30T23:59:59+09:00"),
		},
		{
			name: "2020-04-01",
			now:  timeOf("2020-04-01T00:00:00+09:00"),
			want: timeOf("2020-04-01T00:00:00+09:00"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			util.ClockSource = newMockClock(tt.now)
			if got := util.Yesterday(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Yesterday() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCreateTimePeriod(t *testing.T) {
	type args struct {
		start time.Time
		end   time.Time
	}
	tests := []struct {
		name string
		now  time.Time
		args args
		want util.TimePeriod
	}{
		{
			name: "2020-04-01",
			now:  timeOf("2020-04-01T12:00:00+09:00"),
			args: args{
				start: timeOf("2020-04-01T00:00:00+09:00"),
				end:   timeOf("2020-04-01T12:00:00+09:00"),
			},
			want: util.TimePeriod{
				timeOf("2020-04-01T00:00:00+09:00"),
				timeOf("2020-04-01T12:00:00+09:00"),
			},
		},
		{
			name: "2020-04-02",
			now:  timeOf("2020-04-02T12:00:00+09:00"),
			args: args{
				start: timeOf("2020-04-01T00:00:00+09:00"),
				end:   timeOf("2020-04-02T12:00:00+09:00"),
			},
			want: util.TimePeriod{
				timeOf("2020-04-01T00:00:00+09:00"),
				timeOf("2020-04-02T12:00:00+09:00"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			util.ClockSource = newMockClock(tt.now)
			if got := util.CreateTimePeriod(tt.args.start, tt.args.end); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateTimePeriod() = %v, want %v", got, tt.want)
			}
		})
	}
}

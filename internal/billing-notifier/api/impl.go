package api

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

func NewSchedule(id int32, name string, created time.Time, options ...*ScheduleOption) Schedule {

	c := created.Format(time.UnixDate)

	s := Schedule{
		Id:      &id,
		Name:    &name,
		Created: &c,
	}

	for _, option := range options {
		if option.Crontab != nil {
			s.Crontab = option.Crontab
		}

		if option.Configuration != nil {
			s.Configuration = option.Configuration
		}
	}

	return s
}

type ScheduleOption struct {
	Crontab       *Crontab
	Configuration *Configuration
}

func WithCrontab(year, month, day, hour, minute string) *ScheduleOption {

	return &ScheduleOption{
		Crontab: &Crontab{
			Year:   &year,
			Month:  &month,
			Day:    &day,
			Hour:   &hour,
			Minute: &minute,
		},
	}
}

func WithConfiguration(collector, formatter, notifier string) *ScheduleOption {

	factory := &ComponentFactoryConfiguration{}

	if len(collector) > 0 {
		factory.Collector = &collector
	}

	if len(formatter) > 0 {
		factory.Formatter = &formatter
	}

	if len(notifier) > 0 {
		factory.Notifier = &notifier
	}

	return &ScheduleOption{
		Configuration: &Configuration{
			Factory: factory,
		},
	}
}

func ToSchedule(value interface{}) (*Schedule, error) {

	var b []byte
	switch v := value.(type) {
	case string:
		b = []byte(v)
	case []byte:
		b = v
	default:
		return nil, fmt.Errorf("unknown type: %v", v)
	}

	if !json.Valid(b) {
		return nil, fmt.Errorf("invalid json: %v", b)
	}

	var schedule Schedule
	if err := schedule.UnmarshalBinary(b); err != nil {
		return nil, err
	}

	return &schedule, nil
}

func (s *Schedule) MarshalBinary() ([]byte, error) {

	return json.Marshal(s)
}

func (s *Schedule) UnmarshalBinary(data []byte) error {

	return json.Unmarshal(data, s)
}

func nullSafeCompare(x *string, y *string) int {

	if x == nil {
		if y == nil {
			return 0
		}

		return -1
	}

	return strings.Compare(*x, *y)
}

func (s *Schedule) Compare(y *Schedule) int {

	if c := nullSafeCompare(s.Name, y.Name); c != 0 {
		return c
	}

	if c := s.Crontab.Compare(y.Crontab); c != 0 {
		return c
	}

	if s.Configuration == nil {
		if y.Configuration == nil {
			return 0
		}

		return -1
	}

	if c := s.Configuration.Compare(y.Configuration); c != 0 {
		return c
	}

	return 0
}

func (s *Schedule) CronSpec() string {

	return s.Crontab.CronSpec()
}

func (c *Crontab) Compare(y *Crontab) int {

	if n := nullSafeCompare(c.Year, y.Year); n != 0 {
		return n
	}

	if n := nullSafeCompare(c.Month, y.Month); n != 0 {
		return n
	}

	if n := nullSafeCompare(c.Day, y.Day); n != 0 {
		return n
	}

	if n := nullSafeCompare(c.Hour, y.Hour); n != 0 {
		return n
	}

	if n := nullSafeCompare(c.Minute, y.Minute); n != 0 {
		return n
	}

	return 0
}

func (c *Crontab) CronSpec() string {

	source := struct {
		DayOfWeek  string
		Month      string
		DayOfMonth string
		Hours      string
		Minutes    string
	}{
		DayOfWeek:  "*",
		Month:      "*",
		DayOfMonth: "*",
		Hours:      "*",
		Minutes:    "*",
	}

	if c.Minute != nil {
		source.Minutes = *c.Minute
	}

	if c.Hour != nil {
		source.Hours = *c.Hour
	}

	if c.Day != nil {
		source.DayOfMonth = *c.Day
	}

	if c.Month != nil {
		source.Month = *c.Month
	}

	return fmt.Sprintf("%s %s %s %s %s",
		source.Minutes,
		source.Hours,
		source.DayOfMonth,
		source.Month,
		source.DayOfWeek,
	)
}

func (c *Configuration) Compare(y *Configuration) int {

	if c.Factory == nil {
		if y.Factory == nil {
			return 0
		}

		return -1
	}

	if n := c.Factory.Compare(y.Factory); n != 0 {
		return n
	}

	return 0
}

func (c *ComponentFactoryConfiguration) Compare(y *ComponentFactoryConfiguration) int {

	if y == nil {
		return -1
	}

	if n := nullSafeCompare(c.Collector, y.Collector); n != 0 {
		return n
	}

	if n := nullSafeCompare(c.Formatter, y.Formatter); n != 0 {
		return n
	}

	if n := nullSafeCompare(c.Notifier, y.Notifier); n != 0 {
		return n
	}

	return 0
}

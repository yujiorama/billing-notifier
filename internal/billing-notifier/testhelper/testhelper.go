package testhelper

import (
	"context"

	"github.com/testcontainers/testcontainers-go"
)

type Context struct {
	Container testcontainers.Container
}

func Setup(imageNameTag string, exposedPorts []string) (testcontainers.Container, error) {

	return testcontainers.GenericContainer(context.Background(), testcontainers.GenericContainerRequest{
		ContainerRequest: testcontainers.ContainerRequest{
			Image:        imageNameTag,
			ExposedPorts: exposedPorts,
		},
		Started: true,
	})
}

func TearDown(container testcontainers.Container) error {

	return container.Terminate(context.Background())
}

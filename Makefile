GO_BUILD_ENV := GO111MODULE=on
GO_MODULE_PREFIX := $(shell grep module go.mod | cut -d ' ' -f 2)
SOURCES := $(shell find cmd web/billing-notifier-web internal pkg -type f -name '*.go')

ifeq ($(OS),Windows_NT)
EXTENSION=.exe
else
EXTENSION=
endif

GIT_COMMITISH = $(shell git rev-parse --short=16 HEAD)
GIT_TAG = $(shell git describe --tags --abbrev=0 2>/dev/null)
ifneq ($(strip $(GIT_TAG)),)
VERSION = $(GIT_TAG)
else
ifneq ($(strip $(GIT_COMMITISH)),)
VERSION = $(GIT_COMMITISH)
else
VERSION = debug
endif
endif

.SUFFIX:
.SUFFIX: .go

FORCE:
.PHONY: default all clean tools fmt test bench billing-notifier apicode billing-notifier-web FORCE

default: all

all: billing-notifier billing-notifier-web

tools:
	@sh -c "if ! command -v goimports >/dev/null; then GO111MODULE=off go get -u golang.org/x/tools/cmd/goimports; fi"
	@sh -c "if ! command -v godotenv >/dev/null; then GO111MODULE=off go get -u github.com/joho/godotenv/cmd/godotenv; fi"
	@sh -c "if ! command -v rice >/dev/null; then GO111MODULE=off go get github.com/GeertJohan/go.rice/rice; fi"
	@sh -c "if ! command -v oapi-codegen >/dev/null; then GO111MODULE=off go get github.com/deepmap/oapi-codegen/cmd/oapi-codegen; fi"

fmt: tools FORCE
	@goimports -w $(SOURCES)

test: fmt
	@godotenv go test $(TESTFLAG) -v ./...

bench: fmt
	@godotenv go test -bench -v ./...

billing-notifier: fmt
	@echo $(GIT_COMMITISH) $(GIT_TAG) $(VERSION)
	@rm -rf bin/$(@F)$(EXTENSION)
	$(GO_BUILD_ENV) go build -o bin/$(@F)$(EXTENSION) -v $(GO_MODULE_PREFIX)/cmd/$(@F)
	$(GO_BUILD_ENV) rice append --exec bin/$(@F)$(EXTENSION) --import-path bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/slack/formatter/text

apicode: API_DEFINITION := api/swagger.yaml
apicode: DST_SOURCE_PATH := internal/billing-notifier/api/api.go
apicode: tools
	$(eval TMPDIR := $(shell mktemp --directory | xargs -r cygpath -ma))

	@oapi-codegen -generate types,server -package api -o $(TMPDIR)/api.go $(API_DEFINITION)
	@goimports $(TMPDIR)/api.go > $(DST_SOURCE_PATH)
	@rm -rf $(TMPDIR)
	@ls -l $(DST_SOURCE_PATH)

billing-notifier-web: apicode fmt
	@echo $(GIT_COMMITISH) $(GIT_TAG) $(VERSION)
	@rm -rf bin/$(@F)$(EXTENSION)
	$(GO_BUILD_ENV) go build -o bin/$(@F)$(EXTENSION) -v $(GO_MODULE_PREFIX)/web/billing-notifier-web
	$(GO_BUILD_ENV) rice append --exec bin/$(@F)$(EXTENSION) --import-path bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/slack/formatter/text

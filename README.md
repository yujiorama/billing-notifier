# Billing Notifier

AWS/Azure/GCP の利用料金を Slack で通知します。

## 必要なツールや言語ランタイム

* Go 1.14 以上
* [Heroku Toolbelt](https://toolbelt.heroku.com/)

## ローカルで実行

### CLI

環境変数で指定した設定に応じて動作します。

引数には未対応。

```bash
# ビルド
$ make all

# 実行
$ AWS_DEFAULT_PROFILE=your-profile \
COLLECTOR_TYPE=aws \
NOTIFIER_TYPE=slack \
NOTIFIER_SLACK_WEBHOOK_URL=https://hooks.slack.com/services/aaa/bbb/ccc \
FORMATTER_SLACK_MESSAGE_TYPE=blockkit \
FORMATTER_SLACK_MESSAGE_TEMPLATE=rich \
./bin/billing-nofitier
```

### Web アプリ

* cron 相当のスケジュール実行ができるようになっています
    - つまり、同じ時刻に複数の通知を実行できます
* スケジュールごとに一部の設定パラメータを指定できるようになっています
    - 今のところ COLLECTOR_TYPE  と FORMATTER_TYPE  と NOTIFICATION_TYPE  を指定できます
        + COLLECTOR_TYPE  以外は指定できる以上の意味はありません
    - つまり、単独のアプリケーションインスタンスで複数のクラウドプロバイダーの使用料を通知できます
* http リクエストによる即時実行ができるようになっています
    - 他のシステムでスケジュールを管理しているならそのまま使えばいい

```bash
# ビルド
$ make all

# 実行
$ AWS_DEFAULT_PROFILE=your-profile \
PORT=1374 \
COLLECTOR_TYPE=aws \
NOTIFIER_TYPE=slack \
NOTIFIER_SLACK_WEBHOOK_URL=https://hooks.slack.com/services/aaa/bbb/ccc \
FORMATTER_SLACK_MESSAGE_TYPE=blockkit \
FORMATTER_SLACK_MESSAGE_TEMPLATE=rich \
./bin/billing-nofitier-web
```

## Heroku で実行

* 必要な環境変数は `heroku config:set key=value` で設定します

```bash
heroku create billing-notifier
heroku config:set key=value
```

### CLI

`Procfile` が異なるため `cli` ブランチを push します。

```bash
git push heroku cli:master
heroku run bin/billing-notifier
```

スケジュール実行するときは [Heroku Scheduler](https://elements.heroku.com/addons/scheduler) が便利です。

* Web UI でしか設定できません

```bash
heroku addons:create scheduler:free
heroku addops:open scheduler
```

### Web アプリ

`master` ブランチを push します。

```bash
git push heroku master:master
```

動作確認。

`"billing-notifier"` と表示されたら正常です。

```bash
$ curl -fsSL $(heroku apps:info --json | jq -r .app.web_url)
"billing-notifier"
```

スケジュール設定を Redis に保存するときは [Heroku Redis](https://elements.heroku.com/addons/redis) が便利です。

```bash
heroku addons:create heroku-redis:hobby-dev
heroku config:set REPOSITORY_TYPE=redis
```

## Deploy to Heroku Button

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy?template=https://bitbucket.org/yujiorama/billing-notifier/src/main)

Heroku Button は GitHub リポジトリでしか機能しないので飾りです。

## 準備

### AWS

AWS アカウントの IAM ユーザーが必要です。

IAM ユーザーには `Cost Explorer Service` の `GetCostAndUsage` アクションを実行する権限が必要です。

環境変数の設定によって使用料金の収集対象リージョンは変化します。具体例は次のとおり。

| 環境変数 `AWS_DEFAULT_REGION` | 環境変数 `AWS_PROFILE` | 対象リージョン                                |
|:----------------------------:|:---------------------:|:-------------------------------------------:|
| `ap-northeast-1`             | 指定しない             | `ap-northeast-1` とグローバル                 |
| 指定しない                    | 指定する               | プロファイルに指定された初期リージョンとグローバル |
| 指定しない                    | 指定しない             | (たぶん) `ap-northeast-1` とグローバル         |

#### 環境変数の設定

| 変数名 | 内容 |
|:------|:------|
| `AWS_ACCESS_KEY_ID`            | IAM ユーザーのアクセスキーID。<br/>`AWS_SECRET_ACCESS_KEY` と一緒に指定します。      |
| `AWS_SECRET_ACCESS_KEY`        | IAM ユーザーのシークレットアクセスキー。<br/>`AWS_ACCESS_KEY_ID` と一緒に指定します。 |
| `AWS_PROFILE`                  | プロファイル名。<br/>`AWS_CONFIG_FILE` や `AWS_SHARED_CREDENTIALS_FILE` と一緒に指定します。 |
| `AWS_CONFIG_FILE`              | プロファイル設定ファイルパス。<br/>未指定の場合はユーザーのホームディレクトリを参照します。<br/>`AWS_PROFILE` と一緒に指定します。 |
| `AWS_SHARED_CREDENTIALS_FILE`  | 認証情報ファイルパス。<br/>未指定の場合はユーザーのホームディレクトリを参照します。<br/>`AWS_PROFILE` と一緒に指定します。 |
| `AWS_DEFAULT_REGION`           | AWS サービスの対象リージョン。 |

#### 備考

次のスクリプトは IAM ユーザーを作成・削除・表示します。

```bash
$ ./deployments/aws/setup.sh create
...
AWS_ACCESS_KEY_ID=AKIAIOSFODNN7EXAMPLE
AWS_SECRET_ACCESS_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

$ ./deployments/aws/setup.sh delete

$ ./deployments/aws/setup.sh show
...
AWS_ACCESS_KEY_ID=AKIAIOSFODNN7EXAMPLE
AWS_SECRET_ACCESS_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
```

### Azure

Azure AD に登録したサービスプリンシパルが必要です。

サービスプリンシパルには `[課金データ閲覧者]` のロールを割り当てる必要があります。

#### 環境変数の設定

| 変数名 | 内容 |
|:------|:------|
| `COLLECTOR_AZURE_SUBSCRIPTION_ID` | サブスクリプションID |
| `COLLECTOR_AZURE_TENANT_ID`       | テナントID(ディレクトリID) |
| `COLLECTOR_AZURE_PLAN_ID`         | プランID |
| `COLLECTOR_AZURE_CLIENT_ID`       | クライアントID(アプリケーションID) |
| `COLLECTOR_AZURE_CLIENT_SECRET`   | クライアントシークレット(アプリケーションパスワード) |

#### 備考

次のスクリプトは AD アプリケーションを作成・削除・表示します。

```bash
$ ./deployments/azure/setup.sh create
...
COLLECTOR_AZURE_CLIENT_ID=a4f1217a-6cb3-47f5-b33f-a69630c0b587
COLLECTOR_AZURE_CLIENT_SECRET=69178031-873d-4d9f-9084-94c90858833f
COLLECTOR_AZURE_TENANT_ID=fc9a23ef-9ded-4e68-9752-c7b8c70c5d1f

$ ./deployments/azure/setup.sh delete


$ ./deployments/azure/setup.sh show
...
COLLECTOR_AZURE_CLIENT_ID=a4f1217a-6cb3-47f5-b33f-a69630c0b587
COLLECTOR_AZURE_CLIENT_SECRET=69178031-873d-4d9f-9084-94c90858833f
COLLECTOR_AZURE_TENANT_ID=fc9a23ef-9ded-4e68-9752-c7b8c70c5d1f
```

### GCP

課金データを蓄積するための BigQuery データセット・テーブルと、BigQuery ジョブを実行するためのサービスアカウントが必要です。

サービスアカウントには `roles/bigquer.dataViewer` ロールと `roles/bigquery.user` ロールを割り当てる必要があります。

#### 環境変数の設定

| 変数名 | 内容 |
|:------|:------|
| `COLLECTOR_GCP_PROJECT_ID`                     | プロジェクトID |
| `COLLECTOR_GCP_GOOGLE_APPLICATION_CREDENTIALS` | サービスアカウントの認証情報オブジェクト(JSON文字列) |
| `COLLECTOR_GCP_BILLING_TABLE_NAME`             | BigQuery テーブル名 |

#### 備考

次のスクリプトはサービスアカウントを作成・削除・表示します。

```bash
$ ./deployments/gcp/setup.sh create
...
COLLECTOR_GCP_PROJECT_ID=private-env
COLLECTOR_GCP_GOOGLE_APPLICATION_CREDENTIALS={"type":"service_account",...省略}

$ ./deployments/gcp/setup.sh delete

$ ./deployments/gcp/setup.sh show
...
COLLECTOR_GCP_PROJECT_ID=private-env
COLLECTOR_GCP_GOOGLE_APPLICATION_CREDENTIALS={"type":"service_account",...省略}

```

課金データを BigQuery へエクスポートする設定は Web コンソールでしか実行できません。

[課金データのBigQueryへのエクスポート](https://cloud.google.com/billing/docs/how-to/export-data-bigquery?hl=ja)

### Slack

メッセージを通知する Workspace の Incoming WebHook が必要です。

[Sending messages using Incoming Webhooks](https://api.slack.com/messaging/webhooks) を参考にしてください。

環境変数 `NOTIFIER_SLACK_WEBHOOK_URL` に WebHook URL を指定します。

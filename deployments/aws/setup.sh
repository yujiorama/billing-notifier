#!/bin/bash

AWS="aws"
USER_NAME_PREFIX="billing-notifier"
POLICY_NAME_PREFIX="billing-notifier"
IAM_PATH="/billing-notifier/"

CREDENTIAL_FILE_DIR="${HOME}"

TAG_KEY="App"
TAG_VALUE="billing-notifier"

if [[ "Windows_NT" = "${OS}" ]]; then
  AWS="aws.cmd"
  export MSYS_NO_PATHCONV=1
fi

function check-command {
  if ! command -v "${AWS}" >/dev/null 2>&1; then
    echo "${AWS} コマンドがありません"
    echo "awscli をインストールしてください"
    echo "https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/cli-chap-install.html"
    return 1
  fi

  if ! command -v jq >/dev/null 2>&1; then
    echo "jq コマンドがありません"
    echo "jq をインストールしてください"
    echo "https://github.com/stedolan/jq/wiki/Installation"
    return 1
  fi

  return 0
}

function get-user {

  "${AWS}" iam list-users \
    --path-prefix "${IAM_PATH}" \
    --output text \
    --query 'Users[*].[UserName]' | \
    tr -d '\r' | \
  while read -r user_name; do
    # shellcheck disable=SC2016
    query='Tags[?Key==`'"${TAG_KEY}"'` && Value==`'"${TAG_VALUE}"'`].[Value]'

    found="$("${AWS}" iam list-user-tags \
      --user-name "${user_name}" \
      --output text \
      --query "${query}"
    )"

    if [[ -n "${found}" ]]; then
      echo "${user_name}"
      break
    fi

  done
}

## TODO
function get-role {

  echo ""
  return 0
}

function task-create {
  set -euo pipefail
  local user_name role_name suffix policy_document
  echo "Run task-create"

  user_name="$(get-user)"
  role_name="$(get-role)"
  task-delete "${user_name}" "${role_name}" || return 1

  suffix="$(printf "%08d" "${RANDOM}" | cut -c1-8)"
  user_name="${USER_NAME_PREFIX}-${suffix}"
  "${AWS}" iam create-user \
    --path "${IAM_PATH}" \
    --user-name "${user_name}" \
    --tags Key="${TAG_KEY}",Value="${TAG_VALUE}"

  policy_document="$(cat - <<-EOS | jq -c
{
   "Version": "2012-10-17",
   "Statement": [
       {
           "Effect": "Allow",
           "Action": [
               "ce:GetCostAndUsage"
           ],
           "Resource": [
               "*"
           ]
       }
   ]
}
EOS
)"

  "${AWS}" iam simulate-custom-policy \
    --policy-input-list "${policy_document}" \
    --action-names "ce:GetCostAndUsage" \
    --resource-arns "*"

  policy_name="${POLICY_NAME_PREFIX}-${suffix}"
  "${AWS}" iam put-user-policy \
    --user-name "${user_name}" \
    --policy-name "${policy_name}" \
    --policy-document "${policy_document}"

  "${AWS}" iam create-access-key \
    --user-name "${user_name}" \
    --output json \
    --query 'AccessKey.{AWS_ACCESS_KEY_ID:AccessKeyId, AWS_SECRET_ACCESS_KEY:SecretAccessKey}' \
    > "${CREDENTIAL_FILE_DIR}/${user_name}.json"
}

function task-delete {
  set -euo pipefail
  local user_name
  echo "Run task-delete"

  user_name="$1"
  [[ -z "${user_name}" ]] && return 0

  "${AWS}" iam list-user-policies \
    --user-name "${user_name}" \
    --output text \
    --query 'PolicyNames[*]' | \
    tr -d '\r' | \
  while read -r policy_name; do
    "${AWS}" iam delete-user-policy \
      --user-name "${user_name}" \
      --policy-name "${policy_name}"
  done

  "${AWS}" iam list-access-keys \
    --user-name "${user_name}" \
    --output text \
    --query 'AccessKeyMetadata[*].[AccessKeyId]' | \
    tr -d '\r' | \
  while read -r access_key_id; do
    "${AWS}" iam delete-access-key \
      --user-name "${user_name}" \
      --access-key-id "${access_key_id}"
  done

  "${AWS}" iam delete-user \
    --user-name "${user_name}"

  rm -f "${CREDENTIAL_FILE_DIR}/${user_name}.json"

  return 0
}

function task-show {
  set -euo pipefail
  local user_name
  echo "Run task-show"

  user_name="$1"
  [[ -z "${user_name}" ]] && return 0

  if [[ ! -e "${CREDENTIAL_FILE_DIR}/${user_name}.json" ]]; then
    return 1
  fi

  jq -r 'to_entries|map("\(.key)=\(.value|tostring)") |.[]' < "${CREDENTIAL_FILE_DIR}/${user_name}.json"
}

function aws-setup {
  set -euo pipefail
  local task
  echo "Run aws-setup"

  task="$1"

  check-command || return 1
  case "${task}" in
  create)
    task-create
    task-show "$(get-user)"
    ;;
  delete)
    task-delete "$(get-user)"
    ;;
  show)
    task-show "$(get-user)"
    ;;
  *)
    echo "unknown task: ${task}"
    return 1
    ;;
  esac
}

if [[ "aws-setup" == "$(basename "${BASH_SOURCE[0]}" .sh)" ]]; then
  aws-setup "$@"
fi

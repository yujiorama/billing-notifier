#!/bin/bash

SA_NAME_PREFIX="test-billing-notifier"
CREDENTIAL_FILE_DIR="${HOME}/.gcloud"

if [[ "Windows_NT" = "${OS}" ]]; then
  CREDENTIAL_FILE_DIR="$(readlink -m "${APPDATA}/gcloud")"
fi


function check-command {
  if ! command -v gcloud >/dev/null 2>&1; then
    echo "gcloud コマンドがありません"
    echo "Cloud SDK をインストールしてください"
    echo "https://cloud.google.com/sdk/install"
    return 1
  fi

  if ! command -v bq >/dev/null 2>&1; then
    echo "bq コマンドがありません"
    echo "Cloud SDK をインストールしてください"
    echo "https://cloud.google.com/sdk/install"
    return 1
  fi

  if ! command -v jq >/dev/null 2>&1; then
    echo "jq コマンドがありません"
    echo "jq をインストールしてください"
    echo "https://github.com/stedolan/jq/wiki/Installation"
    return 1
  fi

  return 0
}

function get-project {

  gcloud config get-value project
  return 0
}

function get-sa {
  local sa_id

  sa_id="$(gcloud iam service-accounts list \
    --filter="email:${SA_NAME_PREFIX}* disabled:false" \
    --format="value(email)")"

  echo "${sa_id}"
  return 0
}

function task-create {
  set -euo pipefail
  local project_id sa_id sa_name sa_credential_file sa_credential suffix
  echo "Run task-create"

  sa_id="$(get-sa)"
  task-delete "${sa_id}" || return 1
  project_id="$(get-project)"

  suffix="$(printf "%08d" "${RANDOM}" | cut -c1-8)"
  sa_name="${SA_NAME_PREFIX}-${suffix}"
  gcloud iam service-accounts create \
    "${sa_name}" \
    --display-name "${sa_name}" \
    --project "${project_id}"
  sleep 15

  sa_id="$(get-sa)"
  [[ -z "${sa_id}" ]] && return 1
  gcloud iam service-accounts describe "${sa_id}"

  sa_credential_file="${CREDENTIAL_FILE_DIR}/${sa_id}.json"
  gcloud iam service-accounts keys create \
    "${sa_credential_file}" \
    --iam-account "${sa_id}"
}

function task-delete {
  set -euo pipefail
  local sa_id
  echo "Run task-delete"

  sa_id="$1"
  [[ -z "${sa_id}" ]] && return 0

  gcloud iam service-accounts delete "${sa_id}" --quiet

  sa_credential_file="${CREDENTIAL_FILE_DIR}/${sa_id}.json"
  rm -f "${sa_credential_file}"

  return 0
}

function task-show {
  set -euo pipefail
  local project_id sa_id sa_credential_file sa_credential
  echo "Run task-show"

  project_id="$(get-project)"
  [[ -z "${project_id}" ]] && return 0
  sa_id="$1"
  [[ -z "${sa_id}" ]] && return 0

  sa_credential_file="${CREDENTIAL_FILE_DIR}/${sa_id}.json"
  if [[ ! -e "${sa_credential_file}" ]]; then
    return 1
  fi

  sa_credential="$(jq -c < "${sa_credential_file}")"

  cat - <<-EOS
COLLECTOR_GCP_PROJECT_ID=${project_id}
COLLECTOR_GCP_GOOGLE_APPLICATION_CREDENTIALS=${sa_credential}
EOS

  return 0
}

function gcp-setup {
  set -euo pipefail
  local task
  echo "Run gcp-setup"

  task="$1"

  check-command || return 1

  case "${task}" in
  create)
    task-create
    task-show "$(get-sa)"
    ;;
  delete)
    task-show "$(get-sa)"
    task-delete "$(get-sa)"
    ;;
  show)
    task-show "$(get-sa)"
    ;;
  *)
    echo "unknown task: ${task}"
    return 1
    ;;
  esac
}

if [[ "gcp-setup" == "$(basename "${BASH_SOURCE[0]}" .sh)" ]]; then
  gcp-setup "$@"
fi

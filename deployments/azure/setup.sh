#!/bin/bash

APP_NAME_PREFIX="test-billing-notifier"
BILLING_READER_ROLE_NAME="Billing Reader"

CREDENTIAL_FILE_DIR="${HOME}"

if [[ "Windows_NT" = "${OS}" ]]; then
  export MSYS_NO_PATHCONV=1
fi

function check-command {
  if ! command -v az >/dev/null 2>&1; then
    echo "az コマンドがありません"
    echo "Azure CLI をインストールしてください"
    echo "https://docs.microsoft.com/ja-jp/cli/azure/install-azure-cli?view=azure-cli-latest"
    return 1
  fi

  if ! command -v jq >/dev/null 2>&1; then
    echo "jq コマンドがありません"
    echo "jq をインストールしてください"
    echo "https://github.com/stedolan/jq/wiki/Installation"
    return 1
  fi

  return 0
}

function get-app {
  local app_id

  app_id="$(az ad sp list \
    --filter "startswith(displayName, '${APP_NAME_PREFIX}')" \
    --output tsv \
    --query '[*].appId' | \
    head -n 1)"

  [[ -z "${app_id}" ]] && return 0

  if ! az role assignment list \
    --assignee "${app_id}" \
    --query "[? roleDefinitionName == \`${BILLING_READER_ROLE_NAME}\` ]" >/dev/null 2>&1; then
    return 0
  fi

  echo "${app_id}"
  return 0
}


function task-create {
  set -euo pipefail
  local app_id app_name suffix
  echo "Run task-create"

  app_id="$(get-app)"
  task-delete "${app_id}" || return 1


  suffix="$(printf "%08d" "${RANDOM}" | cut -c1-8)"
  app_name="${APP_NAME_PREFIX}-${suffix}"
  az ad sp create-for-rbac \
    --name "${app_name}" \
    --role "${BILLING_READER_ROLE_NAME}" \
    --sdk-auth false \
    --skip-assignment false \
    --years 3 \
    --output json \
    --query "$(cat - <<-EOS | tr -d '\n'
{
COLLECTOR_AZURE_TENANT_ID:tenant,
COLLECTOR_AZURE_CLIENT_ID:appId,
COLLECTOR_AZURE_CLIENT_SECRET:password
}
EOS
)" > "${CREDENTIAL_FILE_DIR}/${app_name}.json"
}

function task-delete {
  set -euo pipefail
  local app_id app_name
  echo "Run task-delete"

  app_id="$1"
  [[ -z "${app_id}" ]] && return 0

  app_name="$(az ad sp show \
    --id "${app_id}" \
    --output tsv \
    --query 'displayName'
  )"

  az ad sp delete --id "${app_id}"

  rm -f "${CREDENTIAL_FILE_DIR}/${app_name}.json"

  return 0
}

function task-show {
  set -euo pipefail
  local app_id app_name
  echo "Run task-show"

  app_id="$1"
  [[ -z "${app_id}" ]] && return 0

  app_name="$(az ad sp show \
    --id "${app_id}" \
    --output tsv \
    --query 'displayName'
  )"

  if [[ ! -e "${CREDENTIAL_FILE_DIR}/${app_name}.json" ]]; then
    return 1
  fi

  jq -r 'to_entries|map("\(.key)=\(.value|tostring)") |.[]' < "${CREDENTIAL_FILE_DIR}/${app_name}.json"
}

function azure-setup {
  set -euo pipefail
  local task
  echo "Run azure-setup"

  task="$1"

  check-command || return 1
  case "${task}" in
  create)
    task-create
    task-show "$(get-app)"
    ;;
  delete)
    task-delete "$(get-app)"
    ;;
  show)
    task-show "$(get-app)"
    ;;
  *)
    echo "unknown task: ${task}"
    return 1
    ;;
  esac
}

if [[ "azure-setup" == "$(basename "${BASH_SOURCE[0]}" .sh)" ]]; then
  azure-setup "$@"
fi

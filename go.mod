module bitbucket.org/yujiorama/billing-notifier

go 1.15

require (
	cloud.google.com/go/bigquery v1.11.2
	github.com/Azure/azure-sdk-for-go v34.4.0+incompatible
	github.com/Azure/go-autorest/autorest v0.9.8
	github.com/Azure/go-autorest/autorest/adal v0.8.3
	github.com/Azure/go-autorest/autorest/date v0.2.0
	github.com/Azure/go-autorest/autorest/to v0.4.0 // indirect
	github.com/GeertJohan/go.rice v1.0.2
	github.com/PuloV/ics-golang v0.0.0-20190808201353-a3394d3bcade
	github.com/aws/aws-sdk-go v1.35.37
	github.com/channelmeter/iso8601duration v0.0.0-20150204201828-8da3af7a2a61 // indirect
	github.com/deepmap/oapi-codegen v1.7.0
	github.com/getkin/kin-openapi v0.62.0 // indirect
	github.com/go-chi/chi v4.0.2+incompatible // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/go-redis/redis/v7 v7.4.0
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.3.0
	github.com/labstack/gommon v0.3.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/nlopes/slack v0.6.0
	github.com/pkg/errors v0.9.1
	github.com/robfig/cron/v3 v3.0.1
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/testcontainers/testcontainers-go v0.7.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	golang.org/x/sys v0.0.0-20210531225629-47163c9f4e4f // indirect
	golang.org/x/tools v0.1.2 // indirect
	google.golang.org/api v0.32.0
)

// +heroku goVersion go1.15
// +heroku install ./cmd/... ./web/...

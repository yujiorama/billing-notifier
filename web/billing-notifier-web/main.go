package main

import (
	"log"
	"os"
	"strings"

	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/web"

	"github.com/joho/godotenv"
)

func main() {

	log.Println("app.Run start")
	var filenames []string
	if dotenv, ok := os.LookupEnv("DOTENV"); ok {
		for _, s := range strings.Split(dotenv, ",") {
			filenames = append(filenames, strings.TrimSpace(s))
		}
	}
	if err := godotenv.Load(filenames...); err != nil {
		log.Printf("Error loading files [%s]\n", filenames)
	}

	err := web.Run()
	if err != nil {
		log.Fatalf("app.Run Failed: %v", err)
	}

	log.Println("app.Run end")
	os.Exit(0)
}

package cli

import (
	"github.com/pkg/errors"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/util"
	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/aws"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/azure"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/gcp"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/slack/formatter/blockkit"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/slack/formatter/text"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/slack/notifier"
)

func Run() error {

	skipDatePattern, skipDate := util.ParseSkipDatePattern()
	if skipDate != nil && util.Today().Equal(*skipDate) {
		return nil
	}
	if skipDatePattern.IsSkipDate(util.Today()) {
		return nil
	}

	collector, err := domain.ComponentFactory.NewCollector()
	if err != nil {
		return errors.Wrap(err, "NewCollector Failed")
	}

	billingData, err := collector.Collect()
	if err != nil {
		return errors.Wrap(err, "collector.Collect Failed")
	}

	formatter, err := domain.ComponentFactory.NewFormatter()
	if err != nil {
		return errors.Wrap(err, "NewFormatter Failed")
	}

	content, err := formatter.Format(billingData)
	if err != nil {
		return errors.Wrap(err, "formatter.Format Failed")
	}

	notifier, err := domain.ComponentFactory.NewNotifier()
	if err != nil {
		return errors.Wrap(err, "NewNotifier Failed")
	}

	err = notifier.Notify(content)
	if err != nil {
		return errors.Wrap(err, "notifier.Notify Failed")
	}

	return nil
}

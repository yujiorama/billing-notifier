package web

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/api"

	"github.com/robfig/cron/v3"

	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/aws"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/azure"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/gcp"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/redis"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/slack/formatter/blockkit"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/slack/formatter/text"
	_ "bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/slack/notifier"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
)

const (
	defaultListenPort int = 1374
)

func Run() error {

	stdLogger := log.New("billing-notifier")
	stdLogger.SetLevel(log.DEBUG)
	stdLogger.SetOutput(os.Stdout)

	e := echo.New()
	e.HideBanner = true
	e.HidePort = true
	e.Logger.SetLevel(stdLogger.Level())
	e.Logger.SetOutput(stdLogger.Output())

	repository := newScheduleRepository()

	cronTab := cron.New(
		cron.WithLocation(time.UTC),
		cron.WithLogger(
			cron.VerbosePrintfLogger(stdLogger)),
	)
	defer cronTab.Stop()

	apiHandler := newHandler(e.Logger, repository, cronTab)
	api.RegisterHandlers(e, apiHandler)

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(apiHandler.Refresh())

	listenPort := defaultListenPort
	port := os.Getenv("PORT")
	if len(port) > 0 {
		intPort, err := strconv.Atoi(port)
		if err == nil {
			listenPort = intPort
		}
	}

	go func() {
		address := fmt.Sprintf(":%d", listenPort)
		if err := e.Start(address); err != nil {
			e.Logger.Info("shutdown server")
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	return e.Shutdown(ctx)
}

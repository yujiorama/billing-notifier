package web

import (
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/api"

	"github.com/robfig/cron/v3"

	"github.com/labstack/echo/v4"
)

type scheduleHandler struct {
	api.ServerInterface
	echoLogger echo.Logger
	repository *scheduleRepository
	cronTab    *cron.Cron
}

func newHandler(echoLogger echo.Logger, repository *scheduleRepository, cronTab *cron.Cron) *scheduleHandler {

	return &scheduleHandler{
		echoLogger: echoLogger,
		repository: repository,
		cronTab:    cronTab,
	}
}

func (h *scheduleHandler) Get(c echo.Context) error {

	return c.JSON(http.StatusOK, "billing-notifier")
}

func (h *scheduleHandler) GetSchedules(c echo.Context) error {

	schedules, err := h.repository.Values()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("database error: %v", err))
	}

	return c.JSON(http.StatusOK, schedules)
}

func (h *scheduleHandler) GetSchedulesScheduleID(c echo.Context, scheduleID int32) error {

	schedule, err := h.repository.Get(strconv.Itoa(int(scheduleID)))
	if err != nil {
		return echo.NewHTTPError(http.StatusNotFound, fmt.Sprintf("no item: %v", err))
	}

	return c.JSON(http.StatusOK, schedule)
}

func (h *scheduleHandler) PutSchedules(c echo.Context) error {

	var schedules api.Schedules
	if err := c.Bind(&schedules); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Invalid format for request body: %v", err))
	}

	if err := h.repository.Clear(); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("database error: %v", err))
	}

	result := make([]*api.Schedule, len(schedules))
	for i, s := range schedules {
		key := strconv.Itoa(int(*s.Id))
		if err := h.repository.Set(key, &s); err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("database error: %v", err))
		}

		result[i] = &schedules[i]
	}

	return c.JSON(http.StatusCreated, result)
}

func (h *scheduleHandler) PutSchedulesScheduleID(c echo.Context, scheduleID int32) error {

	var schedule api.Schedule
	if err := c.Bind(&schedule); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Invalid format for request body: %v", err))
	}

	if *schedule.Id != scheduleID {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Invalid path param: %v", scheduleID))
	}

	if err := h.repository.Set(strconv.Itoa(int(*schedule.Id)), &schedule); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("database error: %v", err))
	}

	return c.JSON(http.StatusCreated, &schedule)
}

func (h *scheduleHandler) PostTrigger(c echo.Context) error {

	var configuration api.Configuration
	if err := c.Bind(&configuration); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Invalid format for request body: %v", err))
	}

	if configuration.Factory == nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Invalid format for request body: %v", configuration))
	}

	if err := execute(&configuration); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("server error: %v", err))
	}

	return c.JSON(http.StatusOK, "success")
}

func (h *scheduleHandler) Refresh() echo.MiddlewareFunc {

	_ = h.refresh()

	return func(next echo.HandlerFunc) echo.HandlerFunc {

		return func(c echo.Context) error {

			c.Response().After(func() {
				err := h.refresh()
				if err != nil {
					c.Logger().Errorf("refresh error: %w", err)
				}
			})

			return next(c)
		}
	}
}

func (h *scheduleHandler) refresh() error {

	h.cronTab.Start()

	for _, entry := range h.cronTab.Entries() {
		h.cronTab.Remove(entry.ID)
	}

	schedules, err := h.repository.Values()
	if err != nil {
		return fmt.Errorf("values error: %w", err)
	}

	for _, v := range schedules {
		schedule := *v
		h.echoLogger.Infof("name=[%v],crontab=[%v]", *schedule.Name, schedule.CronSpec())
		_, err := h.cronTab.AddFunc(schedule.CronSpec(), func() {

			h.echoLogger.Info(fmt.Sprintf("execute %v", *schedule.Name))
			err := execute(schedule.Configuration)
			if err != nil {
				h.echoLogger.Errorf("execute error: %#v", err)
			}
		})
		if err != nil {
			return fmt.Errorf("addfunc error: %w", err)
		}
	}

	return nil
}

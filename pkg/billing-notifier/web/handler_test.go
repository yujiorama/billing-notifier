package web

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/api"
	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/testhelper"

	"github.com/stretchr/testify/suite"

	"github.com/labstack/echo/v4"
)

type TestContext struct {
	testhelper.Context
	Repository *scheduleRepository
}

type TestSuite struct {
	suite.Suite
}

var testContext TestContext

const (
	redisPort     = "6379/tcp"
	redisProtocol = "redis"
)

func (suite *TestSuite) SetupTest() {

	container, err := testhelper.Setup("redis:alpine", []string{redisPort})
	suite.Require().NotNil(container, "testhelper.Setup(): container")
	suite.Require().Nil(err, "testhelper.Setup()")
	testContext.Container = container

	endpoint, err := testContext.Container.PortEndpoint(context.Background(), redisPort, redisProtocol)
	suite.Require().Nil(err, "Container.PortEndpoint")
	u, err := url.Parse(endpoint)
	suite.Require().Nil(err, "url.Parse")
	suite.Require().Nil(os.Setenv("REPOSITORY_TYPE", "redis"))
	suite.Require().Nil(os.Setenv("HEROKU_REDIS_PINK_URL", fmt.Sprintf("redis://%s:%s", u.Hostname(), u.Port())))
	testContext.Repository = newScheduleRepository()

	dummySchedules := []api.Schedule{
		api.NewSchedule(
			1374,
			"at every 12 o'clock",
			time.Now(),
			api.WithCrontab("*", "*", "*", "12", "0"),
			api.WithConfiguration("aws", "slack", "slack"),
		),
		api.NewSchedule(
			2374,
			"at 2020-03-25 12:34",
			time.Now(),
			api.WithCrontab("2020", "3", "25", "12", "34"),
			api.WithConfiguration("azure", "slack", "default"),
		),
	}

	for _, a := range dummySchedules {
		aScheduleId := strconv.Itoa(int(*a.Id))
		suite.Require().Nil(testContext.Repository.Set(aScheduleId, &a))

		e, err := testContext.Repository.Get(aScheduleId)
		suite.Require().Nil(err)
		suite.Require().Equal(*e, a, "Repository.Set/Repository.Get")
	}
}

func (suite *TestSuite) TearDownTest() {
	suite.Require().NotNil(testContext.Container, "testContext.Container")
	suite.Require().Nil(testhelper.TearDown(testContext.Container), "testhelper.TearDown")
}

func TestHandler(t *testing.T) {
	suite.Run(t, new(TestSuite))
}

func (suite *TestSuite) TestGetSchedules() {
	as := suite.Assert()

	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/schedules", nil)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSONCharsetUTF8)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := scheduleHandler{repository: testContext.Repository}

	if as.NoError(h.GetSchedules(c)) {

		as.Equal(http.StatusOK, rec.Code)

		var actual api.Schedules
		if as.NoError(json.Unmarshal(rec.Body.Bytes(), &actual)) {
			for _, a := range actual {
				if !as.NotNil(a.Id) {
					as.Failf("invalid object", "%v", a)
				}
				id := strconv.Itoa(int(*a.Id))

				e, err := testContext.Repository.Get(id)
				if err != nil {
					as.Error(err, "Repository.Get")
				}
				as.Equal(0, e.Compare(&a))
			}
		} else {
			as.Fail("request error")
		}
	} else {
		as.Fail("GetSchedules failed")
	}
}

func (suite *TestSuite) TestPutSchedules() {

	as := suite.Assert()

	requestBody := `
[
	{
		"id": 1,
		"name": "primary",
		"crontab": {
			"year": "*",
			"month": "*",
			"day": "*",
			"hour": "*",
			"minute": "*"
		},
		"configuration": {
			"factory": {
				"collector": "azure",
				"formatter": "slack",
				"notifier": "slack"
			}
		}
	},
	{
		"id": 2,
		"name": "secondary",
		"crontab": {
			"year": "2020",
			"month": "03",
			"day": "*",
			"hour": "*",
			"minute": "*"
		},
		"configuration": {
			"factory": {
				"collector": "aws",
				"formatter": "default",
				"notifier": "default"
			}
		}
	},
	{
		"id": 3,
		"name": "third",
		"crontab": {
			"year": "2020",
			"month": "1",
			"day": "2",
			"hour": "3",
			"minute": "4"
		},
		"configuration": {
			"factory": {
				"collector": "gcp",
				"formatter": "default",
				"notifier": "default"
			}
		}
	}
]
`

	e := echo.New()
	req := httptest.NewRequest(http.MethodPut, "/schedules", strings.NewReader(requestBody))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSONCharsetUTF8)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := scheduleHandler{repository: testContext.Repository}

	if as.NoError(h.PutSchedules(c)) {

		as.Equal(http.StatusCreated, rec.Code)

		var actual api.Schedules
		if as.NoError(json.Unmarshal(rec.Body.Bytes(), &actual)) {
			for _, a := range actual {
				as.NotNil(a.Id)
				id := strconv.Itoa(int(*a.Id))

				e, err := testContext.Repository.Get(id)
				if err != nil {
					as.Error(err, "Repository.Get")
				}
				as.Equal(0, e.Compare(&a))
			}
		} else {
			as.Fail("request error")
		}
	} else {
		as.Fail("PutSchedules failed")
	}
}

func (suite *TestSuite) TestGetSchedulesScheduleID() {

	as := suite.Assert()

	scheduleID := int32(1374)

	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/schedules/%d", scheduleID), nil)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSONCharsetUTF8)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := scheduleHandler{repository: testContext.Repository}

	if as.NoError(h.GetSchedulesScheduleID(c, scheduleID)) {

		as.Equal(http.StatusOK, rec.Code)

		var actual api.Schedule
		if as.NoError(json.Unmarshal(rec.Body.Bytes(), &actual)) {

			id := strconv.Itoa(int(scheduleID))
			expected, err := testContext.Repository.Get(id)
			if err != nil {
				as.Error(err, "Repository.Get")
			}
			as.Equal(0, expected.Compare(&actual))
		} else {
			as.Fail("Unmarshal error")
		}
	} else {
		as.Fail("GetSchedulesScheduleID failed")
	}
}

func (suite *TestSuite) TestPutSchedulesScheduleID() {

	as := suite.Assert()

	schedule := api.NewSchedule(
		222,
		"TestPutSchedulesScheduleID",
		time.Now(),
		api.WithCrontab("1234", "12", "12", "12", "12"))
	body, err := json.Marshal(&schedule)
	as.Nil(err)

	e := echo.New()
	req := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/schedules/%d", *schedule.Id), bytes.NewReader(body))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSONCharsetUTF8)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := scheduleHandler{repository: testContext.Repository}

	if as.NoError(h.PutSchedulesScheduleID(c, *schedule.Id)) {

		as.Equal(http.StatusCreated, rec.Code)

		var actual api.Schedule
		if as.NoError(json.Unmarshal(rec.Body.Bytes(), &actual)) {

			id := strconv.Itoa(int(*schedule.Id))
			expected, err := testContext.Repository.Get(id)
			if err != nil {
				as.Error(err, "Repository.Get")
			}
			as.Equal(0, expected.Compare(&actual))
		} else {
			as.Fail("request error")
		}
	} else {
		as.Fail("PutSchedulesScheduleID failed")
	}
}

package web

import (
	"fmt"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/api"
	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"
)

type scheduleRepository struct {
	repository domain.Repository
}

func newScheduleRepository() *scheduleRepository {

	repository, err := domain.ComponentFactory.NewRepository()
	if err != nil {
		panic(err)
	}

	return &scheduleRepository{repository: repository}
}

func (r *scheduleRepository) Get(key string) (*api.Schedule, error) {

	value, err := r.repository.Get(key)
	if err != nil {
		return nil, fmt.Errorf("get error: %w", err)
	}

	schedule, err := api.ToSchedule(value)
	if err != nil {
		return nil, fmt.Errorf("convert error: %w", err)
	}

	return schedule, nil
}

func (r *scheduleRepository) Set(key string, schedule *api.Schedule) error {

	err := r.repository.Set(key, schedule)
	if err != nil {
		return fmt.Errorf("set error: %w", err)
	}

	return nil
}

func (r *scheduleRepository) Values() ([]*api.Schedule, error) {

	schedules := make([]*api.Schedule, 0)

	values, err := r.repository.Values()
	if err != nil {
		return schedules, fmt.Errorf("values error: %w", err)
	}

	var xs []*api.Schedule
	for _, v := range values {
		x, err := api.ToSchedule(v)
		if err != nil {
			return schedules, fmt.Errorf("values error: %w", err)
		}

		xs = append(xs, x)
	}

	for _, x := range xs {
		schedules = append(schedules, x)
	}

	return schedules, nil
}

func (r *scheduleRepository) Clear() error {

	err := r.repository.Clear()
	if err != nil {
		return fmt.Errorf("clear error: %w", err)
	}

	return nil
}

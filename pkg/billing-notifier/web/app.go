package web

import (
	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/api"
	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/util"
	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"

	"github.com/pkg/errors"
)

func execute(config *api.Configuration) error {

	skipDatePattern, skipDate := util.ParseSkipDatePattern()
	if skipDate != nil && util.Today().Equal(*skipDate) {
		return nil
	}
	if skipDatePattern.IsSkipDate(util.Today()) {
		return nil
	}

	factory := &api.ComponentFactoryConfiguration{}

	if config.Factory != nil {
		factory = config.Factory
	}

	collector, err := domain.ComponentFactory.NewCollector(factory.Collector)
	if err != nil {
		return errors.Wrap(err, "NewCollector Failed")
	}

	billingData, err := collector.Collect()
	if err != nil {
		return errors.Wrap(err, "collector.Collect Failed")
	}

	formatter, err := domain.ComponentFactory.NewFormatter(factory.Formatter)
	if err != nil {
		return errors.Wrap(err, "NewFormatter Failed")
	}

	content, err := formatter.Format(billingData)
	if err != nil {
		return errors.Wrap(err, "formatter.Format Failed")
	}

	notifier, err := domain.ComponentFactory.NewNotifier(factory.Notifier)
	if err != nil {
		return errors.Wrap(err, "NewNotifier Failed")
	}

	err = notifier.Notify(content)
	if err != nil {
		return errors.Wrap(err, "notifier.Notify Failed")
	}

	return nil
}

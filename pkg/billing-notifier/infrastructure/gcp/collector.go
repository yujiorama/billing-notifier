package gcp

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/util"
	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"
	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/exchangerateapi"

	"cloud.google.com/go/bigquery"
	"github.com/pkg/errors"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

type gcpCollector struct {
	Client    *bigquery.Client
	TableName string
}

func newGcpCollector() (domain.Collector, error) {

	credentialJson := os.Getenv("COLLECTOR_GCP_GOOGLE_APPLICATION_CREDENTIALS")
	projectID := os.Getenv("COLLECTOR_GCP_PROJECT_ID")
	tableName := os.Getenv("COLLECTOR_GCP_BILLING_TABLE_NAME")

	client, err := bigquery.NewClient(context.TODO(), projectID, option.WithCredentialsJSON([]byte(credentialJson)))
	if err != nil {
		return nil, errors.Wrap(err, "bigquery.NewClient Failed")
	}

	return &gcpCollector{Client: client, TableName: tableName}, nil
}

func (collector *gcpCollector) Collect() (*domain.BillingData, error) {

	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()

	billingTable, err := collector.lookupBillingTable(ctx, collector.TableName)
	if err != nil {
		return nil, errors.Wrap(err, "lookupBillingTable failed")
	}

	firstDayOfMonth := util.FirstDayOfMonth()
	yesterday := util.Yesterday()

	queryParam := queryParam{
		table:           billingTable,
		firstDayOfMonth: firstDayOfMonth,
	}

	if err := queryParam.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid query parameter")
	}

	resultSets, err := collector.doQuery(ctx, queryParam)
	if err != nil {
		return nil, errors.Wrap(err, "doQuery failed")
	}

	return transform(resultSets, &firstDayOfMonth, &yesterday), nil
}

func init() {
	domain.ComponentFactory.RegisterCollector("gcp", newGcpCollector)
}

func (collector *gcpCollector) lookupBillingTable(ctx context.Context, tableName string) (*bigquery.Table, error) {

	datasets := collector.Client.Datasets(ctx)
	if datasets == nil {
		return nil, fmt.Errorf("Client.Datasets returns empty")
	}

	for {
		dataset, err := datasets.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, errors.Wrap(err, "DatasetIterator.Next Failed")
		}

		tables := dataset.Tables(ctx)
		if tables == nil {
			return nil, fmt.Errorf("Dataset.Tables returns empty")
		}

		for {
			table, err := tables.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return nil, errors.Wrap(err, "TableIterator.Next Failed")
			}

			if ok := strings.HasSuffix(table.FullyQualifiedName(), tableName); ok {
				return table, nil
			}
		}
	}

	return nil, fmt.Errorf("table=[%s] not found in project=[%s]", tableName, datasets.ProjectID)
}

type queryParam struct {
	table           *bigquery.Table
	firstDayOfMonth time.Time
}

func (q *queryParam) validate() error {

	if q.table == nil {
		return fmt.Errorf("billing export table not found")
	}

	if q.firstDayOfMonth.IsZero() {
		return fmt.Errorf("unspecified billing month")
	}

	return nil
}

func (q *queryParam) tableName() string {

	return fmt.Sprintf("%s.%s.%s", q.table.ProjectID, q.table.DatasetID, q.table.TableID)
}

func (q *queryParam) billingMonth() string {

	return q.firstDayOfMonth.Format("200601")
}

type resultSet struct {
	Month              string
	ProjectName        string
	Location           string
	ServiceDescription string
	Total              float64
	TotalExact         float64
}

func (rs *resultSet) transform() *domain.BillingDataItem {

	rate, err := exchangerateapi.GetRate("USD", "JPY")
	if err != nil {
		log.Printf("exchangerateapi.GetRate failed: %v\n", err)
		log.Printf("continue with fallback value: %v\n", *rate)
	}

	return &domain.BillingDataItem{
		Service: rs.ServiceDescription,
		Amount:  domain.BillingAmount(rs.Total / *rate),
		Rate:    domain.CurrencyRate(*rate),
	}
}

const (
	MONTHLY_BILLING_QUERY_SQL string = `SELECT
	invoice.month,
	IFNULL(project.name, "") as projectname,
	CONCAT(IFNULL(location.location, ""), "|",
		IFNULL(location.country, ""), "|",
		IFNULL(location.region, ""), "|",
		IFNULL(location.zone, "")) as location,
	service.description as servicedescription,
	SUM(cost) + SUM(IFNULL((
		SELECT
		  SUM(c.amount)
		FROM
		  UNNEST(credits) c),
		0)) AS total,
	(SUM(CAST(cost * 1000000 AS int64)) + SUM(IFNULL((
		  SELECT
			SUM(CAST(c.amount * 1000000 AS int64))
		  FROM
			UNNEST(credits) c),
		  0))) / 1000000 AS total_exact
  FROM
	` + "`%s`" + `
  WHERE
    invoice.month = "%s"
  GROUP BY
	1,
	2,
	3,
	4
  ORDER BY
	1 ASC,
	2 ASC,
	3 ASC,
	4 ASC;
	`
)

func (collector *gcpCollector) doQuery(ctx context.Context, queryParam queryParam) ([]resultSet, error) {

	query := collector.Client.Query(fmt.Sprintf(MONTHLY_BILLING_QUERY_SQL, queryParam.tableName(), queryParam.billingMonth()))

	it, err := query.Read(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "Query.Read failed")
	}

	var resultSets []resultSet
	for {
		var rs resultSet
		err := it.Next(&rs)
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, errors.Wrap(err, "QueryIterator.Next failed")
		}
		resultSets = append(resultSets, rs)
	}

	return resultSets, nil
}

func transform(resultSets []resultSet, dateFrom *time.Time, dateTo *time.Time) *domain.BillingData {

	var serviceBillingDataItems []domain.BillingDataItem

	for _, rs := range resultSets {

		dataItem := rs.transform()
		dataItem.DateFrom = dateFrom
		dataItem.DateTo = dateTo

		serviceBillingDataItems = append(serviceBillingDataItems, *dataItem)
	}

	var totalAmount domain.BillingAmount
	var rate domain.CurrencyRate
	for _, dataItem := range serviceBillingDataItems {
		rate = dataItem.Rate
		totalAmount += dataItem.Amount
	}

	return &domain.BillingData{
		CloudServiceName: "GCP",
		ByTotal: &domain.BillingDataItem{
			Service:  "Total",
			Amount:   totalAmount,
			Rate:     rate,
			DateFrom: dateFrom,
			DateTo:   dateTo,
		},
		ByServices: serviceBillingDataItems,
	}
}

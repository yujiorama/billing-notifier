package gcp

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/exchangerateapi"

	"cloud.google.com/go/bigquery"
)

func TestQueryParamValidateFailWhenZeroTime(t *testing.T) {

	queryParam := queryParam{table: &bigquery.Table{}}

	if queryParam.validate() == nil {
		t.Error("when zero time must be fail")
	}
}

func TestQueryParamValidateFailWhenZeroStruct(t *testing.T) {

	queryParam := queryParam{}

	if queryParam.validate() == nil {
		t.Error("when zero reference must be fail")
	}
}

func TestResultSetTransform(t *testing.T) {

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		queryValues := r.URL.Query()
		if queryValues.Get("base") != "USD" {
			t.FailNow()
		}
		if queryValues.Get("symbols") != "JPY" {
			t.FailNow()
		}
		fmt.Fprintln(w, "hello")
	}))
	defer ts.Close()

	exchangerateapi.URL = ts.URL

	rs := resultSet{
		Month:              "201911",
		ProjectName:        "project",
		Location:           "ja",
		ServiceDescription: "BigQuery",
		Total:              1150.0,
		TotalExact:         1374.0,
	}

	actual := rs.transform()

	if rs.ServiceDescription != actual.Service {
		t.Errorf("Service: expected=[%s] actual=[%s]", rs.ServiceDescription, actual.Service)
	}

	if rs.Total != actual.Price() {
		t.Errorf("Price: expected=[%f] actual=[%f]", rs.Total, actual.Price())
	}

	expected := exchangerateapi.FallbackExchangeRate[exchangerateapi.UsdJpy]
	if expected != float64(actual.Rate) {
		t.Errorf("Rate: expected=[%f] actual=[%f]", expected, actual.Rate)
	}
}

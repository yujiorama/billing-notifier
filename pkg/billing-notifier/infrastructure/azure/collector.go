package azure

import (
	"context"
	"fmt"
	"log"
	"strings"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/util"
	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"
	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/exchangerateapi"

	"github.com/pkg/errors"
)

type azureCollector struct {
	UsageAggregatesClient *azureUsageAggregateClient
	RateCardClient        *azureRateCardClient
}

func newAzureCollector() (domain.Collector, error) {

	authorizer, err := newBearerAuthorizer()
	if err != nil {
		return nil, errors.Wrap(err, "newBearerAuthorizer failed")
	}

	usageAggregatesClient, err := newUsageAggregatesClient(authorizer)
	if err != nil {
		return nil, errors.Wrap(err, "newUsageAggregatesClient failed")
	}

	rateCardClient, err := newRateCardClient(authorizer)
	if err != nil {
		return nil, errors.Wrap(err, "newRateCardClient failed")
	}

	return &azureCollector{
		UsageAggregatesClient: usageAggregatesClient,
		RateCardClient:        rateCardClient,
	}, nil
}

func (c *azureCollector) Collect() (*domain.BillingData, error) {

	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()

	timePeriod := util.CreateTimePeriod(util.FirstDayOfMonth(), util.Yesterday())

	billingData, err := c.getBillingData(ctx, timePeriod)
	if err != nil {
		return nil, errors.Wrap(err, "getBillingData failed")
	}
	log.Printf("billingData: %v\n", billingData)

	return billingData, nil
}

func init() {
	domain.ComponentFactory.RegisterCollector("azure", newAzureCollector)
}

func (c *azureCollector) getBillingData(ctx context.Context, timePeriod util.TimePeriod) (*domain.BillingData, error) {

	usageAggregates, err := c.UsageAggregatesClient.Get(ctx, timePeriod, true)
	if err != nil {
		return nil, errors.Wrap(err, "UsageAggregatesClient.GetUsage failed")
	}

	rateCards, err := c.RateCardClient.Get(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "RateCardClient.GetRateCard failed")
	}

	billingData := domain.BillingData{
		CloudServiceName: "AZURE",
	}

	yenToDollar, err := exchangerateapi.GetRate("JPY", "USD")
	if err != nil {
		log.Printf("exchangerateapsapi error: %v\n", err)
	}
	dollarToYen, err := exchangerateapi.GetRate("USD", "JPY")
	if err != nil {
		log.Printf("exchangerateapsapi error: %v\n", err)
	}

	for _, usage := range usageAggregates {
		rateCard := findRateCardOf(rateCards, usage.MeterID)
		if rateCard == nil {
			log.Printf("Unknown MeterID [%s] in RateCard\n", *usage.MeterID)
			continue
		}

		service := fmt.Sprintf("%s:%s:%s:%s", *rateCard.MeterRegion, *rateCard.MeterCategory, *rateCard.MeterSubCategory, *rateCard.MeterName)

		amountYen := *usage.Quantity * *rateCard.MeterRates["0"]

		newItem := domain.BillingDataItem{
			Service: service,
			Amount:  domain.BillingAmount(amountYen * *yenToDollar),
			Rate:    domain.CurrencyRate(*dollarToYen),
		}

		var existItem *domain.BillingDataItem
		for i, v := range billingData.ByServices {
			if strings.EqualFold(v.Service, newItem.Service) {
				billingData.ByServices[i].Amount += newItem.Amount
				existItem = &billingData.ByServices[i]
			}
		}

		if existItem == nil {
			billingData.ByServices = append(billingData.ByServices, newItem)
		}
	}

	totalAmount := 0.0
	for _, item := range billingData.ByServices {
		totalAmount += float64(item.Amount)
	}

	dateFrom := timePeriod.Start()
	dateTo := timePeriod.End()

	billingData.ByTotal = &domain.BillingDataItem{
		Service:  "Total",
		Amount:   domain.BillingAmount(totalAmount),
		Rate:     domain.CurrencyRate(*dollarToYen),
		DateFrom: &dateFrom,
		DateTo:   &dateTo,
	}

	return &billingData, nil
}

func findRateCardOf(rateCards []rateCardData, meterID *string) *rateCardData {

	for _, rateCard := range rateCards {
		if strings.EqualFold(*meterID, rateCard.MeterID.String()) {
			return &rateCard
		}
	}

	return nil
}

package azure

import (
	"os"

	"github.com/Azure/go-autorest/autorest/azure"
)

type configType struct{}

var config configType

func (c *configType) Environment() *azure.Environment {

	azureEnv, _ := azure.EnvironmentFromName("AzurePublicCloud")

	return &azureEnv
}

func (c *configType) UserAgentExtension() string {
	return "billing-notifier/azure-collector"
}

func (c *configType) SubscriptionID() string {

	return os.Getenv("COLLECTOR_AZURE_SUBSCRIPTION_ID")
}

func (c *configType) TenantID() string {

	return os.Getenv("COLLECTOR_AZURE_TENANT_ID")
}

func (c *configType) ClientID() string {
	return os.Getenv("COLLECTOR_AZURE_CLIENT_ID")
}

func (c *configType) ClientSecret() string {

	return os.Getenv("COLLECTOR_AZURE_CLIENT_SECRET")
}

func (c *configType) PlanID() string {

	return os.Getenv("COLLECTOR_AZURE_PLAN_ID")
}

package azure

import (
	"context"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/util"

	"github.com/Azure/azure-sdk-for-go/services/preview/commerce/mgmt/2015-06-01-preview/commerce"
	"github.com/Azure/go-autorest/autorest"
	"github.com/Azure/go-autorest/autorest/date"
)

type azureUsageAggregateClient struct {
	Client *commerce.UsageAggregatesClient
}

type usageData struct {
	commerce.UsageAggregation
}

func newUsageAggregatesClient(authorizer *autorest.BearerAuthorizer) (*azureUsageAggregateClient, error) {

	usageAggregatesClient := commerce.NewUsageAggregatesClient(config.SubscriptionID())
	usageAggregatesClient.Authorizer = authorizer
	if err := usageAggregatesClient.AddToUserAgent(config.UserAgentExtension()); err != nil {
		return nil, err
	}

	return &azureUsageAggregateClient{Client: &usageAggregatesClient}, nil
}

func (c *azureUsageAggregateClient) Get(ctx context.Context, timePeriod util.TimePeriod, showDetails bool) ([]usageData, error) {

	var result []usageData

	// UsageAggregatesAPI は Daily の集計に 00:00:00 を要求する
	reportedStartTime := date.Time{
		Time: time.Date(
			timePeriod.Start().Year(),
			timePeriod.Start().Month(),
			timePeriod.Start().Day(),
			0, 0, 0, 0,
			time.UTC,
		),
	}
	reportedEndTime := date.Time{
		Time: time.Date(
			timePeriod.End().Year(),
			timePeriod.End().Month(),
			timePeriod.End().Day(),
			0, 0, 0, 0,
			time.UTC,
		),
	}

	iter, err := c.Client.ListComplete(ctx, reportedStartTime, reportedEndTime, &showDetails, commerce.Daily, "")
	if err != nil {
		return nil, err
	}
	for iter.NotDone() {
		result = append(result, usageData{iter.Value()})

		if err := iter.NextWithContext(ctx); err != nil {
			return nil, err
		}
	}

	return result, nil
}

package azure

import (
	"context"
	"fmt"
	"strings"

	"github.com/Azure/azure-sdk-for-go/services/preview/commerce/mgmt/2015-06-01-preview/commerce"
	"github.com/Azure/go-autorest/autorest"
)

type azureRateCardClient struct {
	Client *commerce.RateCardClient
}

type rateCardData struct {
	commerce.MeterInfo
}

func newRateCardClient(authorizer *autorest.BearerAuthorizer) (*azureRateCardClient, error) {

	rateCardClient := commerce.NewRateCardClient(config.SubscriptionID())
	rateCardClient.Authorizer = authorizer
	if err := rateCardClient.AddToUserAgent(config.UserAgentExtension()); err != nil {
		return nil, err
	}

	return &azureRateCardClient{Client: &rateCardClient}, nil
}

func (c *azureRateCardClient) Get(ctx context.Context) ([]rateCardData, error) {

	filter := filterOf(config.PlanID(), map[string]string{
		"Currency":   "JPY",
		"Locale":     "ja-JP",
		"RegionInfo": "JP",
	})

	rateCardResult, err := c.Client.Get(ctx, filter)
	if err != nil {
		return nil, err
	}

	if rateCardResult.Meters == nil {
		return nil, fmt.Errorf("rateCardClient.Get returns nil")
	}

	var result []rateCardData
	for _, meter := range *rateCardResult.Meters {
		result = append(result, rateCardData{meter})
	}

	return result, nil
}

func filterOf(offerDurableID string, params map[string]string) string {

	var builder strings.Builder

	if len(offerDurableID) > 0 {
		builder.WriteString(fmt.Sprintf("OfferDurableId eq '%s'", offerDurableID))
	}

	for key, value := range params {
		builder.WriteString(" and ")
		builder.WriteString(fmt.Sprintf("%s eq '%s'", key, value))
	}

	return builder.String()
}

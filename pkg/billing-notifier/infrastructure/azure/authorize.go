package azure

import (
	"github.com/Azure/go-autorest/autorest"
	"github.com/Azure/go-autorest/autorest/adal"
)

func newBearerAuthorizer() (*autorest.BearerAuthorizer, error) {

	oauthConfig, err := adal.NewOAuthConfig(
		config.Environment().ActiveDirectoryEndpoint, config.TenantID())
	if err != nil {
		return nil, err
	}

	token, err := adal.NewServicePrincipalToken(
		*oauthConfig, config.ClientID(), config.ClientSecret(), config.Environment().ResourceManagerEndpoint)
	if err != nil {
		return nil, err
	}

	authorizer := autorest.NewBearerAuthorizer(token)

	return authorizer, nil
}

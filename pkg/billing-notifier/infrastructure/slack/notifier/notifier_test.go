package slack_notifier

import (
	"bytes"
	"io/ioutil"
	"testing"
)

func TestNewSlackRequestSuccessful(t *testing.T) {

	req, err := newSlackRequest().
		withPayload([]byte("Text"), []byte{}).
		withWebhookURL("http://localhost/api").
		build()
	if err != nil {
		t.Errorf("%v", err)
	}
	if req.URL.String() != "http://localhost/api" {
		t.Errorf("actual=[%v] expected[%v]", req.URL, "http://localhost/api")
	}

	if contentType := req.Header.Get("Content-Type"); contentType != "application/json" {
		t.Errorf("actual=[%v] expected[%v]", contentType, "application/json")
	}

	buf, err := ioutil.ReadAll(req.Body)
	if err != nil {
		t.Errorf("%v", err)
	}

	if !bytes.Equal(buf, []byte("Text")) {
		t.Errorf("actual=[%v] expected[%v]", buf, []byte("Text"))
	}
}

func TestNewSlackRequestFailOnInvalidURL(t *testing.T) {

	_, err := newSlackRequest().
		withPayload([]byte("Text"), []byte{}).
		withWebhookURL("").
		build()
	if err == nil {
		t.Fail()
	}
}

func TestNewSlackRequestFailOnMissingPayload(t *testing.T) {

	_, err := newSlackRequest().
		withPayload([]byte{}, []byte{}).
		withWebhookURL("http://localhost/api").
		build()
	if err == nil {
		t.Fail()
	}
}

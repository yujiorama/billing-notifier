package slack_notifier

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"

	"github.com/pkg/errors"
)

type slackNotifier struct {
}

func newSlackNotifier() domain.Notifier {
	return &slackNotifier{}
}

func (notifier *slackNotifier) Notify(content []byte) error {

	req, err := newSlackRequest().
		withPayload(content, []byte("test")).
		withWebhookURL(os.Getenv("NOTIFIER_SLACK_WEBHOOK_URL")).
		build()
	if err != nil {
		return errors.Wrap(err, "request builder failed")
	}

	client := &http.Client{Timeout: 10 * time.Second}

	resp, err := client.Do(req)
	if err != nil {
		return errors.Wrap(err, "http request failed")
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return errors.Wrap(err, "buf.ReadFrom failed")
	}
	if responseBody := buf.String(); responseBody != "ok" {
		return errors.New(fmt.Sprintf("non-ok response returned from Slack: [%s]", responseBody))
	}

	return nil
}

func init() {
	domain.ComponentFactory.RegisterNotifier("slack", func() (domain.Notifier, error) {
		return newSlackNotifier(), nil
	})
}

type slackRequest struct {
	payload    []byte
	webhookURL string
}

func newSlackRequest() *slackRequest {
	return &slackRequest{}
}

func (req *slackRequest) withPayload(payload []byte, fallback []byte) *slackRequest {

	if len(payload) > 0 {
		req.payload = payload
	} else {
		req.payload = fallback
	}

	return req
}

func (req *slackRequest) withWebhookURL(webhookURL string) *slackRequest {

	req.webhookURL = webhookURL

	return req
}

func (req *slackRequest) build() (*http.Request, error) {

	if len(req.payload) == 0 {
		return nil, errors.New("missing Text")
	}

	if len(req.webhookURL) == 0 {
		return nil, errors.New("webhookURL is empty")
	}

	if _, err := url.Parse(req.webhookURL); err != nil {
		return nil, errors.Wrap(err, "webhookURL parse failed")
	}

	httpReq, err := http.NewRequest(http.MethodPost, req.webhookURL, bytes.NewBuffer(req.payload))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create new request")
	}

	httpReq.Header.Add("Content-Type", "application/json")

	return httpReq, nil
}

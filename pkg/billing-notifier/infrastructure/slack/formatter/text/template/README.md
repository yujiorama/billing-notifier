# template

利用可能な変数。

```go
type BillingAmount float64
type CurrencyRate float64
type BillingQuantity float64
type UnitPrice float64

type BillingData struct {
	CloudServiceName string
	ByTotal          *BillingDataItem
	ByServices       []BillingDataItem
}

type BillingDataItem struct {
	Service   string
	Amount    BillingAmount
	Rate      CurrencyRate
	Quantity  *BillingQuantity
	UnitPrice *UnitPrice
	DateFrom  *time.Time
	DateTo    *time.Time
}
```

利用可能な関数。

| 関数          | 機能                                                               |
|:------------:|:------------------------------------------------------------------:|
| `toYYYYMMDD` | `*time.Time` を `YYYYMMDD` 形式の `string` にする                    |
| `toPrice`    | `BillingDataItem` の `Amount` と `Rate` から小計(`float64`)を計算する |

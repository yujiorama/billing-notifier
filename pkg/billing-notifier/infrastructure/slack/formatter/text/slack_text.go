package slack_formatter_text

import (
	"encoding/json"
	"html/template"
	"log"
	"os"
	"strings"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"

	rice "github.com/GeertJohan/go.rice"
	"github.com/pkg/errors"
)

type legacyRequest struct {
	Mrkdwn    bool   `json:"mrkdown"`
	Text      string `json:"text"`
	Channel   string `json:"channel,omitempty"`
	Username  string `json:"username,omitempty"`
	IconEmoji string `json:"icon_emoji,omitempty"`
	IconURL   string `json:"icon_url,omitempty"`
}

type blockKitRequest struct {
	Blocks []interface{} `json:"blocks,omitempty"`
}

type messageType int32

const (
	plainText messageType = iota
	blockKit  messageType = iota
)

type textFormatter struct {
}

func newTextFormatter() domain.Formatter {

	return &textFormatter{}
}

func init() {
	domain.ComponentFactory.RegisterFormatter("slack", func() (domain.Formatter, error) {
		return newTextFormatter(), nil
	})
}

func (f *textFormatter) Format(billingData *domain.BillingData) ([]byte, error) {

	if billingData.ByTotal != nil {
		log.Println(*(billingData.ByTotal))
	}
	for _, data := range billingData.ByServices {
		log.Println(data)
	}

	msgTemplate := getMessageTemplate()
	if len(msgTemplate) == 0 {
		return nil, errors.New("template error")
	}

	text, err := composeMessage(msgTemplate, billingData)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render text")
	}

	msgType := getMessageType()
	return newRequestPayload(msgType, text)
}

func getMessageTemplate() string {

	templateName := "simple"
	if t, ok := os.LookupEnv("FORMATTER_SLACK_MESSAGE_TEMPLATE"); ok {
		templateName = t
	}

	config := rice.Config{
		LocateOrder: []rice.LocateMethod{
			rice.LocateWorkingDirectory,
			rice.LocateEmbedded,
			rice.LocateAppended,
			rice.LocateFS,
		},
	}

	box, err := config.FindBox("template")
	if err != nil {
		log.Printf("box name [template] not found: %v\n", err)
		return ""
	}
	tmpl, err := box.String(templateName)
	if err != nil {
		log.Printf("template [%s] not found: %v\n", templateName, err)
		return ""
	}

	return tmpl
}

func composeMessage(tmpl string, billingData *domain.BillingData) (string, error) {

	t := template.Must(template.New("defaultTemplate").Funcs(
		template.FuncMap{
			"toPrice":    func(item *domain.BillingDataItem) float64 { return item.Price() },
			"toYYYYMMDD": func(t time.Time) string { return t.Format("2006-01-02") },
			"Slice": func(s []domain.BillingDataItem, from int, to int) []domain.BillingDataItem {

				if len(s) < to {
					return s[from:]
				}

				return s[from:to]
			},
			"GreaterThan": func(s []domain.BillingDataItem, i int) bool {
				return len(s) > i
			},
		}).Parse(tmpl))

	var result strings.Builder
	err := t.Execute(&result, billingData)

	return result.String(), err
}

func getMessageType() messageType {

	if t, ok := os.LookupEnv("FORMATTER_SLACK_MESSAGE_TYPE"); ok {
		switch strings.ToLower(t) {
		case "blockkit":
			return blockKit
		default:
			return plainText
		}
	}

	return plainText
}

func newRequestPayload(templateID messageType, text string) ([]byte, error) {

	if blockKit == templateID {

		var v []interface{}
		err := json.Unmarshal([]byte(text), &v)
		if err != nil {
			return nil, err
		}

		request := blockKitRequest{Blocks: v}

		return json.Marshal(&request)
	}

	request := legacyRequest{
		Mrkdwn: true,
		Text:   text,
	}

	if channel, ok := os.LookupEnv("FORMATTER_SLACK_CHANNEL"); ok {
		request.Channel = channel
	}
	if username, ok := os.LookupEnv("FORMATTER_SLACK_USERNAME"); ok {
		request.Username = username
	}
	if iconEmoji, ok := os.LookupEnv("FORMATTER_SLACK_ICON_EMOJI"); ok {
		request.IconEmoji = iconEmoji
	}
	if iconURL, ok := os.LookupEnv("FORMATTER_SLACK_ICON_URL"); ok {
		request.IconURL = iconURL
	}

	return json.Marshal(&request)
}

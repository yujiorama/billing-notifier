package slack_formatter_blockkit

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"

	"github.com/nlopes/slack"
	"github.com/pkg/errors"
)

type blockKitFormatter struct {
}

func newBlockKitFormatter() domain.Formatter {

	return &blockKitFormatter{}
}

func init() {
	domain.ComponentFactory.RegisterFormatter("slack_blockkit", func() (domain.Formatter, error) {
		return newBlockKitFormatter(), nil
	})
}

func (f *blockKitFormatter) Format(billingData *domain.BillingData) ([]byte, error) {

	if billingData.ByTotal != nil {
		log.Println(*(billingData.ByTotal))
	}
	for _, data := range billingData.ByServices {
		log.Println(data)
	}

	message, err := composeMessage(billingData)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render blockkit")
	}

	return newRequestPayload(message)
}

func composeMessage(billingData *domain.BillingData) (*slack.Message, error) {

	blocks := []slack.Block{
		composeHeader("今月の利用料金をお知らせします"),
		slack.NewDividerBlock(),
		composeSummary(billingData.CloudServiceName, billingData.ByTotal),
		slack.NewDividerBlock(),
	}

	for _, detail := range composeDetail(billingData.ByServices) {

		blocks = append(blocks, detail)
	}

	message := slack.NewBlockMessage(
		blocks...,
	)

	return &message, nil
}

func composeHeader(text string) *slack.SectionBlock {

	return slack.NewSectionBlock(
		slack.NewTextBlockObject("mrkdwn", text, false, false),
		nil,
		nil,
	)
}

func composeSummary(serviceName string, total *domain.BillingDataItem) *slack.SectionBlock {

	toYYYYMMDD := func(t *time.Time) string { return t.Format("2006-01-02") }

	summary := []string{"## サマリ", ""}
	summary = append(summary, fmt.Sprintf("*クラウドサービス*     : `%s`", serviceName))
	summary = append(summary, fmt.Sprintf("*期間*               : `%s` から `%s`", toYYYYMMDD(total.DateFrom), toYYYYMMDD(total.DateTo)))
	summary = append(summary, fmt.Sprintf("*合計(円)*           : `%.3f`", total.Price()))
	summary = append(summary, fmt.Sprintf("*合計(USD)*          : `%.3f`", total.Amount))
	summary = append(summary, fmt.Sprintf("*為替レート(JPY/USD)* : `%.3f`", total.Rate))

	return slack.NewSectionBlock(
		slack.NewTextBlockObject("mrkdwn", strings.Join(summary, "\n"), false, false),
		nil,
		nil,
	)
}

func composeDetail(services []domain.BillingDataItem) []*slack.SectionBlock {

	if len(services) == 0 {
		return nil
	}

	details := []string{"## 明細", ""}
	for _, service := range services {

		details = append(details, fmt.Sprintf("*%s* (円) : `%.3f`", service.Service, service.Price()))
	}

	return []*slack.SectionBlock{
		slack.NewSectionBlock(
			slack.NewTextBlockObject("mrkdwn", strings.Join(details, "\n"), false, false),
			nil,
			nil,
		),
	}
}

func newRequestPayload(message *slack.Message) ([]byte, error) {

	if channel, ok := os.LookupEnv("FORMATTER_SLACK_CHANNEL"); ok {
		message.Channel = channel
	}
	if username, ok := os.LookupEnv("FORMATTER_SLACK_USERNAME"); ok {
		message.Username = username
	}
	if iconEmoji, ok := os.LookupEnv("FORMATTER_SLACK_ICON_EMOJI"); ok {
		message.Icons = &slack.Icon{IconEmoji: iconEmoji}
	}
	if iconURL, ok := os.LookupEnv("FORMATTER_SLACK_ICON_URL"); ok {
		message.Icons = &slack.Icon{IconURL: iconURL}
	}

	return json.Marshal(message)
}

package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"reflect"
	"strings"
	"testing"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/testhelper"
	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"

	"github.com/go-redis/redis/v7"

	"github.com/stretchr/testify/suite"
)

type TestContext struct {
	testhelper.Context
	Repository domain.Repository
}

type TestSuite struct {
	suite.Suite
}

var testContext TestContext

const (
	redisPort     = "6379/tcp"
	redisProtocol = "redis"
)

func (suite *TestSuite) SetupTest() {

	container, err := testhelper.Setup("redis:alpine", []string{redisPort})
	suite.Require().NotNil(container, "testhelper.Setup(): container")
	suite.Require().Nil(err, "testhelper.Setup()")
	testContext.Container = container

	endpoint, err := testContext.Container.PortEndpoint(context.Background(), redisPort, redisProtocol)
	suite.Require().Nil(err, "Container.PortEndpoint")
	u, err := url.Parse(endpoint)
	suite.Require().Nil(err, "url.Parse")
	testContext.Repository, err = newRedisRepositoryWithURL(fmt.Sprintf("redis://%s:%s", u.Hostname(), u.Port()))
	suite.Require().Nil(err, "newRedisRepositoryWithURL")
}

func (suite *TestSuite) TearDownTest() {
	suite.Require().NotNil(testContext.Container, "testContext.Container")
	suite.Require().Nil(testhelper.TearDown(testContext.Container), "testhelper.TearDown")
}

func TestRepository(t *testing.T) {
	suite.Run(t, new(TestSuite))
}

func (suite *TestSuite) Test_GetPrimitive() {
	t := suite.T()

	//noinspection GoUnhandledErrorResult
	testContext.Repository.Set("aaa", "bbb")

	type args struct {
		key string
	}
	//noinspection GoPreferNilSlice
	tests := []struct {
		name    string
		args    args
		want    interface{}
		wantErr bool
	}{
		{name: "empty", args: args{key: "k"}, want: "", wantErr: true},
		{name: "string", args: args{key: "aaa"}, want: "bbb", wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := testContext.Repository
			got, err := r.Get(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Get() got = (%T)%+v, specified (%T)%+v", got, got, tt.want, tt.want)
			}
		})
	}
}

func (suite *TestSuite) Test_GetComplex() {
	t := suite.T()

	var ar interface{}
	ar = &Persons{
		{Name: "hello"},
	}

	//noinspection GoUnhandledErrorResult
	testContext.Repository.Set("bbb", ar)

	type args struct {
		key string
	}
	//noinspection GoPreferNilSlice
	tests := []struct {
		name    string
		args    args
		want    interface{}
		wantErr bool
	}{
		{name: "complex", args: args{key: "bbb"}, want: ar, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := testContext.Repository
			v, err := r.Get(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			var got Persons
			if err := json.Unmarshal([]byte(v.(string)), &got); err != nil {
				t.Errorf("Unmarshal error = %v", err)
			}
			if !reflect.DeepEqual(&got, tt.want) {
				t.Errorf("Get() got = (%T)%+v, specified (%T)%+v", &got, &got, tt.want, tt.want)
			}
		})
	}
}

func (suite *TestSuite) Test_SetPrimitive() {
	t := suite.T()

	aaa := "123"
	bbb := "xyz"

	type args struct {
		key   string
		value interface{}
	}
	//noinspection GoPreferNilSlice
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "int", args: args{key: "aaa", value: aaa}, wantErr: false},
		{name: "string", args: args{key: "bbb", value: bbb}, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := testContext.Repository
			if err := r.Set(tt.args.key, tt.args.value); (err != nil) != tt.wantErr {
				t.Errorf("Set() error = %v, wantErr %v", err, tt.wantErr)
			}
			got, err := r.Get(tt.args.key)
			if err != nil || err == redis.Nil {
				t.Errorf("Get() error = %v", err)
			}
			if !reflect.DeepEqual(got, tt.args.value) {
				t.Errorf("Get() got = (%T)%+v, specified (%T)%+v", got, got, tt.args.value, tt.args.value)
			}
		})
	}
}

func (suite *TestSuite) Test_SetComplex() {
	t := suite.T()

	var ar interface{}
	ar = &Persons{
		{Name: "hello"},
	}

	type args struct {
		key   string
		value interface{}
	}
	//noinspection GoPreferNilSlice
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "complex", args: args{key: "ccc", value: ar}, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := testContext.Repository
			if err := r.Set(tt.args.key, tt.args.value); (err != nil) != tt.wantErr {
				t.Errorf("Set() error = %v, wantErr %v", err, tt.wantErr)
			}
			v, err := r.Get(tt.args.key)
			if err != nil || err == redis.Nil {
				t.Errorf("Get() error = %v", err)
			}
			var got Persons
			if err := json.Unmarshal([]byte(v.(string)), &got); err != nil {
				t.Errorf("Unmarshal error = %v", err)
			}
			if !reflect.DeepEqual(&got, tt.args.value) {
				t.Errorf("Get() got = (%T)%+v, specified (%T)%+v", &got, &got, tt.args.value, tt.args.value)
			}
		})
	}
}

func (suite *TestSuite) Test_ScanComplex() {
	t := suite.T()

	ar := []Person{
		{Name: "first"},
		{Name: "second"},
		{Name: "third"},
	}

	type args struct {
		key   string
		value interface{}
	}
	//noinspection GoPreferNilSlice
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "first", args: args{key: "first", value: &ar[0]}, wantErr: false},
		{name: "second", args: args{key: "second", value: &ar[1]}, wantErr: false},
		{name: "third", args: args{key: "third", value: &ar[2]}, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := testContext.Repository
			if err := r.Set(tt.args.key, tt.args.value); (err != nil) != tt.wantErr {
				t.Errorf("Set() error = %v, wantErr %v", err, tt.wantErr)
			}
			v, err := r.Get(tt.args.key)
			if err != nil || err == redis.Nil {
				t.Errorf("Get() error = %v", err)
			}
			var got Person
			if err := json.Unmarshal([]byte(v.(string)), &got); err != nil {
				t.Errorf("Unmarshal error = %v", err)
			}
			if !reflect.DeepEqual(&got, tt.args.value) {
				t.Errorf("Get() got = (%T)%+v, specified (%T)%+v", &got, &got, tt.args.value, tt.args.value)
			}
		})
	}

	t.Run("scan", func(t *testing.T) {
		r := testContext.Repository
		values, err := r.Values()
		if err != nil {
			t.Errorf("Values() error = %v", err)
		}
		if len(values) != len(ar) {
			t.Errorf("mismatch values: got:%d, expected:%d", len(values), len(ar))
		}

		for i, v := range values {
			if v == nil {
				t.Errorf("values[%d] is nil", i)
			}
			var got Person
			if err := json.Unmarshal([]byte(v.(string)), &got); err != nil {
				t.Errorf("Unmarshal error = %v", err)
			}

			var expected Person
			for _, a := range ar {
				if strings.Compare(a.Name, got.Name) == 0 {
					expected = a
					break
				}
			}

			if expected.Name == "" {
				t.Errorf("unknown person: %v", got)
			}

			if !reflect.DeepEqual(&got, &expected) {
				t.Errorf("got = (%T)%+v, specified (%T)%+v", &got, &got, &expected, &expected)
			}
		}

	})
}

type Person struct {
	Name string `json:"name"`
}

type Persons []Person

func (p *Person) MarshalBinary() ([]byte, error) {

	return json.Marshal(*p)
}

func (p *Persons) MarshalBinary() ([]byte, error) {

	return json.Marshal(*p)
}

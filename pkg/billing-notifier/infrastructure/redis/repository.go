package redis

import (
	"log"
	"net/url"
	"os"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"

	"github.com/go-redis/redis/v7"
)

const (
	emptyPassword string = ""
	defaultDB     int    = 0
	defaultPort   int    = 6379
)

type redisRepository struct {
	Client   *redis.Client
	RedisURL string
}

func newRedisRepository() (domain.Repository, error) {

	return newRedisRepositoryWithURL(os.Getenv("HEROKU_REDIS_PINK_URL"))
}

func newRedisRepositoryWithURL(redisURL string) (domain.Repository, error) {

	u, err := url.Parse(redisURL)
	if err != nil {
		panic(err)
	}

	options := redis.Options{
		Addr:               u.Host,
		Password:           emptyPassword,
		DB:                 defaultDB,
		DialTimeout:        3 * time.Second,
		ReadTimeout:        3 * time.Second,
		WriteTimeout:       3 * time.Second,
		IdleTimeout:        60 * time.Second,
		IdleCheckFrequency: 30 * time.Second,
	}
	if u.User != nil {
		if passwd, ok := u.User.Password(); ok {
			options.Password = passwd
		}
	}
	client := redis.NewClient(&options)

	log.Println(redisURL, "ping")
	pong, err := client.Ping().Result()
	log.Println(redisURL, pong)

	return &redisRepository{Client: client, RedisURL: redisURL}, err
}

func (r *redisRepository) Get(key string) (interface{}, error) {

	if err := r.checkConnection(); err != nil {
		return nil, err
	}

	return r.Client.Get(key).Result()
}

func (r *redisRepository) Set(key string, value interface{}) error {

	if err := r.checkConnection(); err != nil {
		return err
	}

	return r.Client.Set(key, value, 0).Err()
}

func (r *redisRepository) Values() ([]interface{}, error) {

	if err := r.checkConnection(); err != nil {
		return nil, err
	}

	var cursor uint64
	result := make([]interface{}, 0, 10)
	for {
		var keys []string
		var err error
		keys, cursor, err = r.Client.Scan(cursor, "*", 10).Result()
		if err != nil {
			return nil, err
		}
		for _, key := range keys {
			if v, err := r.Get(key); err == nil {
				result = append(result, v)
			}
		}

		if cursor == 0 {
			break
		}
	}

	return result, nil
}

func (r *redisRepository) Clear() error {

	if err := r.checkConnection(); err != nil {
		return err
	}

	return r.Client.FlushAll().Err()
}

func init() {
	domain.ComponentFactory.RegisterRepository("redis", newRedisRepository)
}

func (r *redisRepository) checkConnection() error {

	log.Println(r.RedisURL, "ping")
	pong, err := r.Client.Ping().Result()
	log.Println(r.RedisURL, pong)

	return err
}

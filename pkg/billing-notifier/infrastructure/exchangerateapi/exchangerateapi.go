package exchangerateapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
)

const (
	httpRequestTimeoutSec time.Duration = time.Duration(30 * time.Second)
)

type ExchangeRateType int64

const (
	UsdJpy ExchangeRateType = iota
	JpyUsd ExchangeRateType = iota
)

func ExchangeRateOf(base string, target string) ExchangeRateType {

	if strings.EqualFold(base, "USD") && strings.EqualFold(target, "JPY") {
		return UsdJpy
	}
	if strings.EqualFold(base, "JPY") && strings.EqualFold(target, "USD") {
		return JpyUsd
	}

	return UsdJpy
}

var (
	FallbackExchangeRate map[ExchangeRateType]float64 = map[ExchangeRateType]float64{
		UsdJpy: 115.0,
		JpyUsd: 0.009092,
	}
	URL      string = "https://api.exchangeratesapi.io/"
	apiCache map[url.URL]float64
)

func init() {
	apiCache = make(map[url.URL]float64)
}

type exchangeRateApiResult struct {
	Rates map[string]float64 `json:"rates"`
	Base  string             `json:"base"`
	Date  string             `json:"date"`
}

func GetRate(base string, target string) (*float64, error) {

	fallback := float64(FallbackExchangeRate[ExchangeRateOf(base, target)])

	url, err := url.Parse(URL)
	if err != nil {
		return &fallback, errors.Wrap(err, "url.Parse failed")
	}
	url.Path = "latest"
	query := url.Query()
	query.Add("base", base)
	query.Add("symbols", target)
	url.RawQuery = query.Encode()

	cachedRate, ok := apiCache[*url]
	if ok {
		return &cachedRate, nil
	}

	req, err := http.NewRequest(http.MethodGet, url.String(), nil)
	if err != nil {
		return &fallback, errors.Wrap(err, "http.NewRequest failed")
	}

	client := &http.Client{Timeout: httpRequestTimeoutSec}
	resp, err := client.Do(req)
	if err != nil {
		return &fallback, errors.Wrap(err, "client.Do failed")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &fallback, errors.Wrap(err, "ioutil.ReadAll failed")
	}

	var exchangeRateResult exchangeRateApiResult
	if err := json.Unmarshal(body, &exchangeRateResult); err != nil {
		return &fallback, errors.Wrap(err, fmt.Sprintf("json.Unmarshal failed: %v", string(body)))
	}

	rate := exchangeRateResult.Rates[target]
	apiCache[*url] = float64(rate)
	return &rate, nil
}

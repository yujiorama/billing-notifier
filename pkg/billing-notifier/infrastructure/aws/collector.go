package aws

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/internal/billing-notifier/util"
	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/domain"
	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/exchangerateapi"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/costexplorer"
	"github.com/pkg/errors"
)

type awsCollector struct {
	Client *costexplorer.CostExplorer
}

func newAwsCollector() (domain.Collector, error) {

	// 環境変数を優先するが ~/.aws/{config,credentials} も参照する
	sess, err := session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	})
	if err != nil {
		return nil, errors.Wrap(err, "session.NewSessionWithOptions Failed")
	}

	return &awsCollector{Client: costexplorer.New(sess)}, nil
}

func (c *awsCollector) Collect() (*domain.BillingData, error) {

	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()

	interval := createDateInterval(util.FirstDayOfMonth(), util.Yesterday())

	billingData, err := c.getBillingData(ctx, interval)
	if err != nil {
		return nil, errors.Wrap(err, "getBillingData failed")
	}
	log.Printf("billingData: %v\n", billingData)

	return billingData, nil
}

func init() {
	domain.ComponentFactory.RegisterCollector("aws", newAwsCollector)
}

var (
	metricsAmortizedCost         = "AmortizedCost"
	metricsBlendedCost           = "BlendedCost"
	metricsNetAmortizedCost      = "NetAmortizedCost"
	metricsNetUnblendedCost      = "NetUnblendedCost"
	metricsNormalizedUsageAmount = "NormalizedUsageAmount"
	metricsUnblendedCost         = "UnblendedCost"
	metricsUsageQuantity         = "UsageQuantity"
)

func (c *awsCollector) getBillingData(ctx context.Context, dateInterval *costexplorer.DateInterval) (*domain.BillingData, error) {

	monthlyTotalBilling := createGetCostAndUsageInput(dateInterval, costexplorer.GranularityMonthly, metricsAmortizedCost, nil)

	opts := request.WithLogLevel(aws.LogDebug)

	totalBilling, err := c.getTotalBilling(ctx, monthlyTotalBilling, opts)
	if err != nil {
		return nil, errors.Wrap(err, "getTotalBilling failed")
	}

	groupBy := newGroupDefinition(costexplorer.DimensionService, costexplorer.GroupDefinitionTypeDimension)
	monthlyServicesBillingInput := createGetCostAndUsageInput(dateInterval, costexplorer.GranularityMonthly, metricsAmortizedCost, groupBy)

	servicesBilling, err := c.getServicesBilling(ctx, monthlyServicesBillingInput, opts)
	if err != nil {
		return nil, errors.Wrap(err, "getServicesBilling failed")
	}

	billingData := &domain.BillingData{
		CloudServiceName: "AWS",
		ByTotal:          totalBilling,
	}

	for _, data := range servicesBilling {
		billingData.ByServices = append(billingData.ByServices, *data)
	}

	return billingData, nil
}

func (c *awsCollector) getTotalBilling(ctx context.Context, input *costexplorer.GetCostAndUsageInput, opts request.Option) (*domain.BillingDataItem, error) {

	costAndUsage, err := c.Client.GetCostAndUsageWithContext(ctx, input, opts)
	if err != nil {
		return nil, errors.Wrap(err, "GetCostAndUsageWithContext failed")
	}

	result := costAndUsage.ResultsByTime[0]

	costMetric := input.Metrics[0]

	amount, err := amountOf(result.Total[*costMetric])
	if err != nil {
		return nil, errors.Wrap(err, "parse Amount as float failed")
	}

	rate, err := rateOf(result.Total[*costMetric])
	if err != nil {
		return nil, errors.Wrap(err, "convert Unit to rate failed")
	}

	dateFrom, err := dateOf(result.TimePeriod.Start)
	if err != nil {
		return nil, errors.Wrap(err, "parse TimePeriod.Start as time failed")
	}
	dateTo, err := dateOf(result.TimePeriod.End)
	if err != nil {
		return nil, errors.Wrap(err, "parse TimePeriod.End as time failed")
	}

	return &domain.BillingDataItem{
		Service:  "Total",
		Amount:   domain.BillingAmount(amount),
		Rate:     domain.CurrencyRate(rate),
		DateFrom: &dateFrom,
		DateTo:   &dateTo,
	}, nil
}

func (c *awsCollector) getServicesBilling(ctx context.Context, input *costexplorer.GetCostAndUsageInput, opts request.Option) ([]*domain.BillingDataItem, error) {

	costAndUsage, err := c.Client.GetCostAndUsageWithContext(ctx, input, opts)
	if err != nil {
		return nil, errors.Wrap(err, "GetCostAndUsageWithContext failed")
	}

	var servicesBilling []*domain.BillingDataItem

	costMetric := input.Metrics[0]

	for idx, group := range costAndUsage.ResultsByTime[0].Groups {
		amount, err := amountOf(group.Metrics[*costMetric])
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("parse Amount as float failed at [%d]", idx))
		}

		rate, err := rateOf(group.Metrics[*costMetric])
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("convert Unit to rate failed at [%d]", idx))
		}

		service := group.Keys[0]

		billingDataItem := &domain.BillingDataItem{
			Service: *service,
			Amount:  domain.BillingAmount(amount),
			Rate:    domain.CurrencyRate(rate),
		}

		servicesBilling = append(servicesBilling, billingDataItem)
	}
	return servicesBilling, nil
}

func newGroupDefinition(key string, dimension string) *costexplorer.GroupDefinition {

	return &costexplorer.GroupDefinition{
		Key:  &key,
		Type: &dimension,
	}
}

func amountOf(value *costexplorer.MetricValue) (float64, error) {

	if value.Amount == nil {
		return -1, fmt.Errorf("Amount is nil")
	}

	amount := value.Amount

	return strconv.ParseFloat(*amount, 64)
}

func rateOf(value *costexplorer.MetricValue) (float64, error) {

	if value.Unit == nil {
		return -1, fmt.Errorf("Unit is nil")
	}

	unit := value.Unit

	rate, err := exchangerateapi.GetRate(*unit, "JPY")
	if err != nil {
		log.Printf("exchangerateapi.GetRate failed: %v\n", err)
		log.Printf("continue with fallback value: %v\n", *rate)
	}

	return *rate, nil
}

func dateOf(date *string) (time.Time, error) {

	return time.Parse("2006-01-02", *date)
}

func createGetCostAndUsageInput(dateInterval *costexplorer.DateInterval,
	granularity string, costMetric string, groupDefinition *costexplorer.GroupDefinition) *costexplorer.GetCostAndUsageInput {

	getCostAndUsageInput := &costexplorer.GetCostAndUsageInput{}

	input := getCostAndUsageInput.
		SetTimePeriod(dateInterval).
		SetGranularity(granularity).
		SetMetrics([]*string{&costMetric})

	if groupDefinition != nil {
		input.SetGroupBy([]*costexplorer.GroupDefinition{groupDefinition})
	}

	return input
}

func createDateInterval(start time.Time, end time.Time) *costexplorer.DateInterval {

	timePeriod := &costexplorer.DateInterval{}

	return timePeriod.
		SetStart(fmt.Sprintf("%04d-%02d-%02d", start.Year(), start.Month(), start.Day())).
		SetEnd(fmt.Sprintf("%04d-%02d-%02d", end.Year(), end.Month(), end.Day()))
}

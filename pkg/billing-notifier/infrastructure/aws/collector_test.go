package aws

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"bitbucket.org/yujiorama/billing-notifier/pkg/billing-notifier/infrastructure/exchangerateapi"

	"github.com/aws/aws-sdk-go/service/costexplorer"
)

func TestAmountOfFoundError(t *testing.T) {

	v := &costexplorer.MetricValue{}

	actual, err := amountOf(v)

	if err == nil {
		t.Fail()
	}

	if actual > 0 {
		t.Fail()
	}
}

func TestAmountOfFailedOnNonFloatString(t *testing.T) {

	illegalString := "illegal"
	v := &costexplorer.MetricValue{Amount: &illegalString}

	actual, err := amountOf(v)

	if err == nil {
		t.Fail()
	}

	if actual > 0 {
		t.Fail()
	}
}

func TestAmountOfSuccess(t *testing.T) {

	floatString := "11.99"
	v := &costexplorer.MetricValue{Amount: &floatString}

	actual, err := amountOf(v)

	if err != nil {
		t.Errorf("%v", err)
	}

	if actual < 0 {
		expected, _ := strconv.ParseFloat(floatString, 64)
		t.Errorf("expected=[%v] actual=[%v]", expected, actual)
	}

	intString := "111"
	v = &costexplorer.MetricValue{Amount: &intString}

	actual, err = amountOf(v)

	if err != nil {
		t.Errorf("%v", err)
	}

	if actual < 0 {
		expected, _ := strconv.ParseFloat(intString, 64)
		t.Errorf("expected=[%v] actual=[%v]", expected, actual)
	}

}

func TestRateOfFoundError(t *testing.T) {
	v := &costexplorer.MetricValue{}

	actual, err := rateOf(v)

	if err == nil {
		t.Fail()
	}

	if actual > 0 {
		t.Fail()
	}
}

func TestRateOfFailedOnUnknownCurrencyUnit(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		queryValues := r.URL.Query()
		if queryValues.Get("base") != "111" {
			t.FailNow()
		}
		if queryValues.Get("symbols") != "JPY" {
			t.FailNow()
		}
		fmt.Fprintln(w, "hello")
	}))
	defer ts.Close()

	exchangerateapi.URL = ts.URL

	unknownCurrencyUnit := "111"
	v := &costexplorer.MetricValue{Unit: &unknownCurrencyUnit}

	actual, err := rateOf(v)

	if err != nil {
		t.Fail()
	}

	expected := exchangerateapi.FallbackExchangeRate[exchangerateapi.UsdJpy]
	if expected != actual {
		t.Errorf("expected=[%v] actual=[%v]", expected, actual)
	}
}

func TestRateOfSuccess(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		queryValues := r.URL.Query()
		if queryValues.Get("base") != "USD" {
			t.FailNow()
		}
		if queryValues.Get("symbols") != "JPY" {
			t.FailNow()
		}
		fmt.Fprintln(w, `{"rates":{"JPY":108.5516870065},"base":"USD","date":"2019-10-18"}`)
	}))
	defer ts.Close()

	exchangerateapi.URL = ts.URL

	currencyUnit := "USD"
	v := &costexplorer.MetricValue{Unit: &currencyUnit}

	actual, err := rateOf(v)

	if err != nil {
		t.Errorf("%v", err)
	}

	expected := float64(108.5516870065)
	if expected != actual {
		t.Errorf("expected=[%v] actual=[%v]", expected, actual)
	}
}

func TestDateOf(t *testing.T) {

	dateString := "2019-01-12"
	actual, err := dateOf(&dateString)

	if err != nil {
		t.Errorf("%v", err)
	}

	if actual.Year() != 2019 {
		t.Errorf("year: expected=[%v] actual=[%v]", 2019, actual.Year())
	}

	if actual.Month() != 1 {
		t.Errorf("month: expected=[%v] actual=[%v]", 2019, actual.Month())
	}

	if actual.Day() != 12 {
		t.Errorf("day: expected=[%v] actual=[%v]", 2019, actual.Day())
	}
}

func TestCreateTimePeriod(t *testing.T) {

	startTime := time.Date(2019, 1, 1, 0, 0, 0, 0, time.Local)
	endTime := time.Date(2019, 1, 23, 0, 0, 0, 0, time.Local)

	actual := createDateInterval(startTime, endTime)

	if "2019-01-01" != *(actual.Start) {
		t.Errorf("expected=[%v] actual=[%v]", "2019-01-01", *(actual.Start))
	}

	if "2019-01-23" != *(actual.End) {
		t.Errorf("expected=[%v] actual=[%v]", "2019-01-23", *(actual.End))
	}
}

func TestCreateGetCostAndUsageInput(t *testing.T) {

	timePeriod := createDateInterval(
		time.Date(2019, 1, 1, 0, 0, 0, 0, time.Local),
		time.Date(2019, 1, 23, 0, 0, 0, 0, time.Local),
	)

	actual := createGetCostAndUsageInput(timePeriod, "granularity", "costMetric", nil)

	if timePeriod.Start != actual.TimePeriod.Start {
		t.Errorf("expected=[%v] actual=[%v]", *(timePeriod.Start), *(actual.TimePeriod.Start))
	}

	if timePeriod.End != actual.TimePeriod.End {
		t.Errorf("expected=[%v] actual=[%v]", *(timePeriod.End), *(actual.TimePeriod.End))
	}

	if "granularity" != *(actual.Granularity) {
		t.Errorf("expected=[%v] actual=[%v]", "granularity", *(actual.Granularity))
	}

	if len(actual.Metrics) != 1 {
		t.Errorf("length of Metrics expected=[%v] actual=[%v]", 1, len(actual.Metrics))
	}

	if "costMetric" != *(actual.Metrics[0]) {
		t.Errorf("expected=[%v] actual=[%v]", "costMetric", *(actual.Metrics[0]))
	}

	if len(actual.GroupBy) != 0 {
		t.Errorf("unknown GruopBy [%v]", actual.GroupBy)
	}

	groupBy := newGroupDefinition(costexplorer.DimensionService, costexplorer.GroupDefinitionTypeDimension)
	actual = createGetCostAndUsageInput(timePeriod, "granularity", "costMetric", groupBy)

	if len(actual.GroupBy) != 1 {
		t.Errorf("unknown GruopBy [%v]", actual.GroupBy)
	}

	if costexplorer.DimensionService != *(actual.GroupBy[0].Key) {
		t.Errorf("expected=[%v] actual=[%v]", costexplorer.DimensionService, *(actual.GroupBy[0].Key))
	}

	if costexplorer.GroupDefinitionTypeDimension != *(actual.GroupBy[0].Type) {
		t.Errorf("expected=[%v] actual=[%v]", costexplorer.GroupDefinitionTypeDimension, *(actual.GroupBy[0].Type))
	}

}

package domain

import "time"

type Collector interface {
	Collect() (*BillingData, error)
}

type collectorFactory func() (Collector, error)

type defaultCollector struct {
}

func (f *defaultCollector) Collect() (*BillingData, error) {

	dateFrom := time.Date(2019, time.October, 1, 0, 0, 0, 0, time.Local)
	dateTo := dateFrom.AddDate(0, 1, 0).Add(-1 * time.Second)

	quantity := BillingQuantity(456)
	unitPrice := UnitPrice(50)

	return &BillingData{
		CloudServiceName: "default",
		ByTotal: &BillingDataItem{
			Service:  "Total",
			Amount:   BillingAmount(10),
			Rate:     CurrencyRate(115),
			DateFrom: &dateFrom,
			DateTo:   &dateTo,
		},
		ByServices: []BillingDataItem{
			{
				Service:  "サービス1",
				Amount:   BillingAmount(123),
				Rate:     CurrencyRate(115),
				DateFrom: &dateFrom,
				DateTo:   &dateTo,
			},
			{
				Service:   "サービス2",
				Quantity:  &quantity,
				UnitPrice: &unitPrice,
				DateFrom:  &dateFrom,
				DateTo:    &dateTo,
			},
		},
	}, nil
}

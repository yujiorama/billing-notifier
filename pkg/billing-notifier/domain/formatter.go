package domain

import (
	"strings"
)

type Formatter interface {
	Format(billingData *BillingData) ([]byte, error)
}

type formatterFactory func() (Formatter, error)

type defaultFormatter struct {
}

func (f *defaultFormatter) Format(billingData *BillingData) ([]byte, error) {

	var result strings.Builder

	if billingData.ByTotal != nil {
		result.WriteString("---- ByTotal\n")
		result.WriteString(billingData.ByTotal.String())
		result.WriteString("\n")
	}

	result.WriteString("---- ByService\n")
	for _, data := range billingData.ByServices {
		result.WriteString(data.String())
		result.WriteString("\n")
	}

	return []byte(result.String()), nil
}

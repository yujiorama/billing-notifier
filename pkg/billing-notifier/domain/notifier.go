package domain

import "log"

type Notifier interface {
	Notify(content []byte) error
}

type notifierFactory func() (Notifier, error)

type defaultNotifier struct {
}

func (c *defaultNotifier) Notify(content []byte) error {
	log.Println(string(content))
	return nil
}

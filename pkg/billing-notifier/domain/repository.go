package domain

import (
	"encoding"
	"errors"
	"fmt"
)

type Repository interface {
	Get(key string) (interface{}, error)
	Set(key string, value interface{}) error
	Values() ([]interface{}, error)
	Clear() error
}

type repositoryFactory func() (Repository, error)

type defaultRepository struct {
}

var defaultRepositoryDB = make(map[string][]byte)

func (r *defaultRepository) Get(key string) (interface{}, error) {

	return defaultRepositoryDB[key], nil
}

func (r *defaultRepository) Set(key string, value interface{}) error {

	if v, ok := value.(encoding.BinaryMarshaler); ok {
		b, err := v.MarshalBinary()
		if err != nil {
			return fmt.Errorf("marshal binary error: %w", err)
		}

		defaultRepositoryDB[key] = b
		return nil
	}

	return errors.New("value must implement encoding.BinaryMarshaler")
}

func (r *defaultRepository) Values() ([]interface{}, error) {

	values := make([]interface{}, 0, len(defaultRepositoryDB))
	for _, v := range defaultRepositoryDB {
		values = append(values, v)
	}

	return values, nil
}

func (r *defaultRepository) Clear() error {

	defaultRepositoryDB = make(map[string][]byte)

	return nil
}

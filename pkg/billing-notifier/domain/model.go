package domain

import (
	"fmt"
	"time"
)

type BillingData struct {
	CloudServiceName string
	ByTotal          *BillingDataItem
	ByServices       []BillingDataItem
}

type BillingAmount float64
type CurrencyRate float64
type BillingQuantity float64
type UnitPrice float64

type BillingDataItem struct {
	Service   string
	Amount    BillingAmount
	Rate      CurrencyRate
	Quantity  *BillingQuantity
	UnitPrice *UnitPrice
	DateFrom  *time.Time
	DateTo    *time.Time
}

func (data BillingDataItem) String() string {

	return fmt.Sprintf("Service=[%s], Price=[%f], DateFrom=[%v], DateTo=[%v]",
		data.Service,
		data.Price(),
		data.DateFrom,
		data.DateTo,
	)
}

func (data BillingDataItem) Price() float64 {
	if data.Quantity != nil && data.UnitPrice != nil {
		return float64(*data.Quantity) * float64(*data.UnitPrice)
	}

	return float64(data.Amount) * float64(data.Rate)
}

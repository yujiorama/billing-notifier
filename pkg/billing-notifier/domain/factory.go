package domain

import (
	"os"
)

var (
	ComponentFactory = newComponentFactory()
)

type componentFactory struct {
	CollectorFactory  map[string]collectorFactory
	FormatterFactory  map[string]formatterFactory
	NotifierFactory   map[string]notifierFactory
	RepositoryFactory map[string]repositoryFactory
}

func newComponentFactory() *componentFactory {

	c := componentFactory{}

	c.RegisterCollector("default", func() (Collector, error) {
		return &defaultCollector{}, nil
	})

	c.RegisterFormatter("default", func() (Formatter, error) {
		return &defaultFormatter{}, nil
	})

	c.RegisterNotifier("default", func() (Notifier, error) {
		return &defaultNotifier{}, nil
	})

	c.RegisterRepository("default", func() (Repository, error) {
		return &defaultRepository{}, nil
	})

	return &c
}

func (factory *componentFactory) RegisterCollector(key string, fn collectorFactory) {

	if factory.CollectorFactory == nil {
		factory.CollectorFactory = make(map[string]collectorFactory)
	}

	factory.CollectorFactory[key] = fn
}

func (factory *componentFactory) NewCollector(param ...*string) (Collector, error) {

	var key string

	if len(param) > 0 {
		if param[0] != nil {
			key = *param[0]
		}
	}

	if len(key) == 0 {
		if env, ok := os.LookupEnv("COLLECTOR_TYPE"); ok {
			key = env
		}
	}

	if len(key) == 0 {
		key = "default"
	}

	if f, ok := factory.CollectorFactory[key]; ok {

		return f()
	}

	return factory.CollectorFactory["default"]()
}

func (factory *componentFactory) RegisterFormatter(key string, fn formatterFactory) {

	if factory.FormatterFactory == nil {
		factory.FormatterFactory = make(map[string]formatterFactory)
	}

	factory.FormatterFactory[key] = fn
}

func (factory *componentFactory) NewFormatter(param ...*string) (Formatter, error) {

	var key string

	if len(param) > 0 {
		if param[0] != nil {
			key = *param[0]
		}
	}

	if len(key) == 0 {
		if env, ok := os.LookupEnv("FORMATTER_TYPE"); ok {
			key = env
		}
	}

	if len(key) == 0 {
		key = "default"
	}

	if f, ok := factory.FormatterFactory[key]; ok {

		return f()
	}

	return factory.FormatterFactory["default"]()
}

func (factory *componentFactory) RegisterNotifier(key string, fn notifierFactory) {

	if factory.NotifierFactory == nil {
		factory.NotifierFactory = make(map[string]notifierFactory)
	}

	factory.NotifierFactory[key] = fn
}

func (factory *componentFactory) NewNotifier(param ...*string) (Notifier, error) {

	var key string

	if len(param) > 0 {
		if param[0] != nil {
			key = *param[0]
		}
	}

	if len(key) == 0 {
		if env, ok := os.LookupEnv("NOTIFIER_TYPE"); ok {
			key = env
		}
	}

	if len(key) == 0 {
		key = "default"
	}

	if f, ok := factory.NotifierFactory[key]; ok {

		return f()
	}

	return factory.NotifierFactory["default"]()
}

func (factory *componentFactory) RegisterRepository(key string, fn repositoryFactory) {

	if factory.RepositoryFactory == nil {
		factory.RepositoryFactory = make(map[string]repositoryFactory)
	}

	factory.RepositoryFactory[key] = fn
}

func (factory *componentFactory) NewRepository(param ...string) (Repository, error) {

	var key string

	if len(param) > 0 {
		key = param[0]
	}

	if len(key) == 0 {
		if env, ok := os.LookupEnv("REPOSITORY_TYPE"); ok {
			key = env
		}
	}

	if len(key) == 0 {
		key = "default"
	}

	if f, ok := factory.RepositoryFactory[key]; ok {

		return f()
	}

	return factory.RepositoryFactory["default"]()
}
